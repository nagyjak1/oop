package filters

trait Filter[T] {

  def filter ( image: T ): T

}
