package filters.greyscale.invert

import filters.greyscale.GreyscaleImageFilter
import models.grids.arrayPixelGrids.GreyscalePixelGrid
import models.images.GreyscaleImage
import models.pixels.GreyscalePixel

class InvertFilter extends GreyscaleImageFilter {
  override def filter(image: GreyscaleImage): GreyscaleImage = {

    val height = image.height()
    val width = image.width()
    val resultPixelGrid = new GreyscalePixelGrid(height, width)

    for (i <- 0 until height) {
      for (j <- 0 until width)
        resultPixelGrid.setPixel(j, i, new GreyscalePixel(255 - image.pixelGrid().getPixel(j, i).getValue))
    }

    new GreyscaleImage(resultPixelGrid)
  }
}
