package filters.greyscale.flip

object FlipDirectionConvertor {

  def convert(value: String): FlipDirection = {

    value match {

      case "x" =>
        XFlip()

      case "y" =>
        YFlip()

      case _ =>
        throw new IllegalArgumentException("Invalid flip direction argument - valid arguments are 'x' or 'y'")
    }

  }
}
