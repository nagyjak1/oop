package filters.greyscale.flip

sealed trait FlipDirection

case class XFlip() extends FlipDirection
case class YFlip() extends FlipDirection
