package filters.greyscale.flip

import filters.greyscale.GreyscaleImageFilter
import models.grids.arrayPixelGrids.GreyscalePixelGrid
import models.images.GreyscaleImage
import models.pixels.GreyscalePixel


class FlipFilter(flipDirection: FlipDirection) extends GreyscaleImageFilter {

  override def filter(image: GreyscaleImage): GreyscaleImage = {

    val height = image.height()
    val width = image.width()
    val resultPixelGrid = new GreyscalePixelGrid(height, width)

    for (i <- 0 until height) {
      for (j <- 0 until width) {
        flipDirection match {
          case XFlip() => resultPixelGrid.setPixel(j, height - 1 - i, new GreyscalePixel(image.pixelGrid().getPixel(j, i).getValue))
          case YFlip() => resultPixelGrid.setPixel(width - 1 - j, i, new GreyscalePixel(image.pixelGrid().getPixel(j, i).getValue))
        }
      }
    }
    new GreyscaleImage(resultPixelGrid)
  }
}
