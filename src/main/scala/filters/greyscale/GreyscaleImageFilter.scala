package filters.greyscale

import filters.Filter
import models.images.GreyscaleImage

trait GreyscaleImageFilter extends Filter[GreyscaleImage] {

}
