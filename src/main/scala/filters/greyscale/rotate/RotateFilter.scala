package filters.greyscale.rotate

import filters.greyscale.GreyscaleImageFilter
import models.grids.arrayPixelGrids.GreyscalePixelGrid
import models.images.GreyscaleImage
import models.pixels.GreyscalePixel

class RotateFilter(rotateAngle: RotateAngle) extends GreyscaleImageFilter {
  override def filter(image: GreyscaleImage): GreyscaleImage = {

    val height = image.height()
    val width = image.width()

    rotateAngle match {
      case RotateAngle90() => val resultPixelGrid = new GreyscalePixelGrid(width, height)

        for (i <- 0 until height) {
          for (j <- 0 until width)
            resultPixelGrid.setPixel(resultPixelGrid.width() - 1 - i, j, new GreyscalePixel(image.pixelGrid().getPixel(j, i).getValue))
        }
        new GreyscaleImage(resultPixelGrid)

      case RotateAngle180() => val rotate90 = new RotateFilter(RotateAngle90())
        rotate90.filter(rotate90.filter(image))

      case RotateAngle270() => val rotate90 = new RotateFilter(RotateAngle90())
        rotate90.filter(rotate90.filter(rotate90.filter(image)))

      case RotateAngle0() =>
        val resultPixelGrid = new GreyscalePixelGrid(height, width)
        for (i <- 0 until height) {
          for (j <- 0 until width)
            resultPixelGrid.setPixel(j, i, new GreyscalePixel(image.pixelGrid().getPixel(j, i).getValue))
        }
        new GreyscaleImage(resultPixelGrid);
    }
  }
}
