package filters.greyscale.rotate

object RotateAngleConvertor {

  def convert(value: Int): RotateAngle = {

    var angle = value % 360
    if (angle < 0)
      angle += 360

    angle match {
      case 0 =>
        RotateAngle0()
      case 90 =>
        RotateAngle90()
      case 180 =>
        RotateAngle180()
      case 270 =>
        RotateAngle270()
      case _ =>
        throw new IllegalArgumentException("Invalid rotate angle argument - valid arguments are multiples of 90 or multiples of -90")
    }

  }
}
