package filters.greyscale.rotate

sealed trait RotateAngle

case class RotateAngle0() extends RotateAngle
case class RotateAngle90() extends RotateAngle
case class RotateAngle180() extends RotateAngle
case class RotateAngle270() extends RotateAngle

