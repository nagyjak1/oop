package filters.greyscale.brightness

import filters.greyscale.GreyscaleImageFilter
import models.grids.arrayPixelGrids.GreyscalePixelGrid
import models.images.GreyscaleImage
import models.pixels.GreyscalePixel

class BrightnessFilter(value: Int) extends GreyscaleImageFilter {
  override def filter(image: GreyscaleImage): GreyscaleImage = {

    val height = image.height()
    val width = image.width()
    val resultPixelGrid = new GreyscalePixelGrid(height, width)

    for (i <- 0 until height) {
      for (j <- 0 until width) {
        val oldGreyscalePixel = image.pixelGrid().getPixel(j, i).getValue
        val newGreyscalePixel = new GreyscalePixel(oldGreyscalePixel + value)
        resultPixelGrid.setPixel(j, i, newGreyscalePixel)
      }
    }
    new GreyscaleImage(resultPixelGrid)
  }
}
