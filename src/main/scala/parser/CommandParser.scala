package parser

import convertors.greyscaleToASCII.{DefaultLinearConvertor, DefaultNonLinearConvertor, LinearConvertor}
import exporters.ascii.{FileExporter, StdOutputExporter}
import filters.greyscale.brightness.BrightnessFilter
import filters.greyscale.flip.{FlipDirectionConvertor, FlipFilter}
import filters.greyscale.invert.InvertFilter
import filters.greyscale.rotate.{RotateAngleConvertor, RotateFilter}
import handlers._
import importers.rgb.RandomImageImporter
import importers.rgb.file.DefaultFileTypeImporter

class CommandParser {

  var exportHandler: Option[ExportHandler] = None

  var importHandler: Option[ImportHandler] = None

  var filterHandler: Option[FilterHandler] = None

  var conversionHandler: Option[ConversionHandler] = None

  def parse(args: List[String]): Unit = {

    var arguments: List[String] = args
    var importFound: Boolean = false
    var tableSet: Boolean = false

    while (arguments.nonEmpty) {
      arguments match {

        // IMPORT
        case "--image" :: path :: tail =>
          if (importFound)
            throw new IllegalArgumentException("More than one import argument")
          importHandler = Some(new ImportHandler(new DefaultFileTypeImporter(path)))
          arguments = tail
          importFound = true;

        case "--image-random" :: tail =>
          if (importFound)
            throw new IllegalArgumentException("More than one import argument")
          importHandler = Some(new ImportHandler(new RandomImageImporter()))
          arguments = tail
          importFound = true;


        // FILTERS
        case "--rotate" :: degrees :: tail =>
          val handler = new FilterHandler(new RotateFilter(RotateAngleConvertor.convert(degrees.toInt)))
          filterHandler = Some(setNextHandler(filterHandler, handler).asInstanceOf[FilterHandler])
          arguments = tail

        case "--invert" :: tail =>
          val handler = new FilterHandler(new InvertFilter())
          filterHandler = Some(setNextHandler(filterHandler, handler).asInstanceOf[FilterHandler])
          arguments = tail

        case "--flip" :: direction :: tail =>
          val handler = new FilterHandler(new FlipFilter(FlipDirectionConvertor.convert(direction)))
          filterHandler = Some(setNextHandler(filterHandler, handler).asInstanceOf[FilterHandler])
          arguments = tail

        case "--brightness" :: value :: tail =>
          val handler = new FilterHandler(new BrightnessFilter(value.toInt))
          filterHandler = Some(setNextHandler(filterHandler, handler).asInstanceOf[FilterHandler])
          arguments = tail


        // EXPORT
        case "--output-file" :: path :: tail =>
          val handler = new ExportHandler(new FileExporter(path))
          exportHandler = Some(setNextHandler(exportHandler, handler).asInstanceOf[ExportHandler])
          arguments = tail

        case "--output-console" :: tail =>
          val handler = new ExportHandler(new StdOutputExporter())
          exportHandler = Some(setNextHandler(exportHandler, handler).asInstanceOf[ExportHandler])
          arguments = tail


        // TABLE SETTING
        case "--table" :: name :: tail =>
          if (tableSet)
            throw new IllegalArgumentException("More than one table argument")
          name match {
            case "linear" =>
              conversionHandler = Some(new ConversionHandler(new DefaultLinearConvertor()))
            case "non-linear" =>
              conversionHandler = Some(new ConversionHandler(new DefaultNonLinearConvertor()))
            case _ =>
              throw new IllegalArgumentException("Invalid table name - supported names are linear or non-linear")
          }
          tableSet = true
          arguments = tail

        case "--custom-table" :: table :: tail =>
          if (tableSet)
            throw new IllegalArgumentException("More than one table argument")
          conversionHandler = Some(new ConversionHandler(new LinearConvertor( table )))
          tableSet = true
          arguments = tail

        // OTHER UNDEFINED ARGUMENTS
        case _ =>
          throw new IllegalArgumentException("Invalid argument")
      }
    }
    if ( !tableSet )
      conversionHandler = Some(new ConversionHandler(new DefaultLinearConvertor()))

    if ( importHandler.isEmpty )
      throw new IllegalArgumentException("No image argument found")
  }

  private def setNextHandler[I,O](handler: Option[Handler[I, O]], newHandler: Handler[I, O]): Handler[I, O] = {

    if (handler.isEmpty)
      newHandler
    else {
      var currentHandler = handler.get
      while (currentHandler.nextHandler.nonEmpty)
        currentHandler = currentHandler.nextHandler.get
      currentHandler.setNext(newHandler)
      handler.get
    }
  }

}
