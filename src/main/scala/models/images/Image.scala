package models.images

import models.grids.AbstractPixelGrid


abstract class Image[T <: AbstractPixelGrid](pixelGrid: T) {

  def pixelGrid(): T = pixelGrid

  def height(): Int = pixelGrid.height()

  def width(): Int = pixelGrid.width()
}
