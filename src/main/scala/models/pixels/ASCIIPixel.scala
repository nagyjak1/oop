package models.pixels

class ASCIIPixel(var ch: Char) extends Pixel[Char] {
  override def getValue: Char = ch

  override def setValue(value: Char): Unit = {
    ch = value
  }
}
