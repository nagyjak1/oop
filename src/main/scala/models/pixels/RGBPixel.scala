package models.pixels

class RGBPixel(var value: Int) extends Pixel[Int] {

  def greyScale: Int = {
    val greyScale = (0.3 * red) + (0.59 * green) + (0.11 * blue)
    if (greyScale >= 255) 255 else greyScale.round.toInt
  }

  def red: Int = (value & 0xFF0000) >> 16

  def green: Int = (value & 0x00FF00) >> 8

  def blue: Int = value & 0x0000FF

  override def getValue: Int = value

  override def setValue(value: Int): Unit = {
    this.value = value
  }
}
