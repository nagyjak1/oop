package models.pixels

class GreyscalePixel(var greyscaleVal: Int) extends Pixel[Int] {

  if ( greyscaleVal < 0 )
    greyscaleVal = 0
  if ( greyscaleVal > 255)
    greyscaleVal = 255

  override def getValue: Int = greyscaleVal

  override def setValue(value: Int): Unit = {
    greyscaleVal = value
  }
}
