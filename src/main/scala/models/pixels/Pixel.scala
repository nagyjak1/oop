package models.pixels

trait Pixel[T] {
  def getValue: T

  def setValue(value: T): Unit
}
