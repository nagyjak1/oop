package models.grids.arrayPixelGrids

import models.pixels.GreyscalePixel

class GreyscalePixelGrid(height: Int, width: Int) extends PixelGridWithArray[GreyscalePixel](height, width) {

}
