package models.grids.arrayPixelGrids

import models.pixels.ASCIIPixel

class ASCIIPixelGrid(height: Int, width: Int) extends PixelGridWithArray[ASCIIPixel](height, width) {

}
