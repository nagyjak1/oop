package models.grids.arrayPixelGrids

import models.grids.PixelGrid

import scala.reflect.ClassTag

class PixelGridWithArray[T](height: Int, width: Int)(implicit m: ClassTag[T]) extends PixelGrid[T](height, width) {

  private val grid: Array[T] = new Array[T](height * width)

  override def setPixel(x: Int, y: Int, value: T): Unit = {
    grid.update(getArrayIndex(x, y), value)
  }

  override def getPixel(x: Int, y: Int): T = {
    grid(getArrayIndex(x, y))
  }

  private def getArrayIndex(x: Int, y: Int): Int =  x + width * y
}

