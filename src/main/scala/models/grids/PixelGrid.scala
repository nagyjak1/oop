package models.grids

abstract class PixelGrid[T] (height: Int, width: Int) extends AbstractPixelGrid {

  require(height > 0)
  require(width > 0)

  def height(): Int = height

  def width(): Int = width

  def setPixel(x: Int, y: Int, value: T): Unit

  def getPixel(x: Int, y: Int): T

}
