package models.grids

trait AbstractPixelGrid {

  def height(): Int

  def width(): Int

}
