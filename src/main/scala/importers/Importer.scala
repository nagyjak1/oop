package importers

trait Importer[T] {
  def `import`(): T
}
