package importers.rgb

import importers.Importer
import models.images.RGBImage

trait RGBImageImporter extends Importer[RGBImage] {

}
