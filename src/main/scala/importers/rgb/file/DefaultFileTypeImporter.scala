package importers.rgb.file

import models.grids.arrayPixelGrids.RGBPixelGrid
import models.images.RGBImage
import models.pixels.RGBPixel

import java.io.File
import javax.imageio.ImageIO

class DefaultFileTypeImporter(path: String) extends FileImporter(path) {

  require( path.endsWith(".jpg") || path.endsWith(".png") || path.endsWith(".gif") )

  override def `import`(): RGBImage = {

    val file = new File (path)
    val bufferImage = ImageIO.read( file )

    val height = bufferImage.getHeight
    val width = bufferImage.getWidth

    val pixelGrid = new RGBPixelGrid(height, width)

    for (i <- 0 until height) {
      for (j <- 0 until width)
        pixelGrid.setPixel(j, i, new RGBPixel(bufferImage.getRGB(j, i)))
    }

    new RGBImage(pixelGrid)
  }
}
