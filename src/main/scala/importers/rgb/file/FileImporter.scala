package importers.rgb.file

import importers.rgb.RGBImageImporter

import java.io.File

abstract class FileImporter(path: String) extends RGBImageImporter {

  def file(): File = new File(path)

  require(file().exists() && file().canRead)

  def path(): String = path
}
