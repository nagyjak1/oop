package importers.rgb

import models.grids.arrayPixelGrids.RGBPixelGrid
import models.images.RGBImage
import models.pixels.RGBPixel

import scala.util.Random

class RandomImageImporter() extends RGBImageImporter {

  def this(seed: Long){
    this()
    Random.setSeed(seed)
  }

  override def `import`(): RGBImage = {

    val height = Random.between(20, 700)
    val width = Random.between(20, 700)

    val pixelGrid = new RGBPixelGrid(height, width)

    for (i <- 0 until height) {
      for (j <- 0 until width)
        pixelGrid.setPixel(j, i, new RGBPixel(Random.between(0xff000000, 0xffffffff)))
    }

    new RGBImage(pixelGrid)
  }
}
