import convertors.RGBToGreyscale.RGBToGreyscaleConvertor
import parser.CommandParser

import scala.sys.exit

object Main extends App {

  val greyscaleConvertor = new RGBToGreyscaleConvertor()
  val parser = new CommandParser()

  try {
    parser.parse(args.toList)
  } catch {
    case e: IllegalArgumentException =>
      println("Program arguments are not correct:\n" + e.toString)
      exit(1)
  }

  val RGBImage = parser.importHandler.get.handle()

  var greyscaleImage = greyscaleConvertor.convert(RGBImage)

  if ( parser.filterHandler.nonEmpty )
    greyscaleImage = parser.filterHandler.get.handle( greyscaleImage )

  val ASCIIImage = parser.conversionHandler.get.handle( greyscaleImage )

  if ( parser.exportHandler.nonEmpty)
    parser.exportHandler.get.handle( ASCIIImage )
}
