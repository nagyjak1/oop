package exporters

trait Exporter[T] {
  def export(image: T): Unit
}
