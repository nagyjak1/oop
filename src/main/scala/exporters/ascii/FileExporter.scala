package exporters.ascii

import java.io.{File, FileOutputStream}

class FileExporter(path: String) extends StreamExporter(new FileOutputStream(new File(path))) {
  require(new File(path).canWrite)
}
