package exporters.ascii

import convertors.ASCIIToString.ASCIIToStringConvertor
import models.images.ASCIIImage

import java.io.OutputStream

class StreamExporter(outputStream: OutputStream) extends ASCIIImageExporter {

  private var closed = false

  private def close(): Unit = {
    if (closed) return

    outputStream.close()
    closed = true
  }

  override def export(image: ASCIIImage): Unit = {

    if (closed) throw new IllegalStateException("This stream is closed")

    val convertor = new ASCIIToStringConvertor()
    outputStream.write(convertor.convert( image ).getBytes("UTF-8") )
    outputStream.flush()

    close()
  }
}
