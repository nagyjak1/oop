package exporters.ascii

import exporters.Exporter
import models.images.ASCIIImage

trait ASCIIImageExporter extends Exporter[ASCIIImage] {

}
