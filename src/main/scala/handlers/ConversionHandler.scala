package handlers

import convertors.greyscaleToASCII.GreyscaleToASCIIConvertor
import models.images.{ASCIIImage, GreyscaleImage}

class ConversionHandler(convertor: GreyscaleToASCIIConvertor) extends Handler[GreyscaleImage, ASCIIImage] {

  override def handle(value: GreyscaleImage): ASCIIImage = {
    convertor.convert(value)
  }
}
