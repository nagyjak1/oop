package handlers

import filters.greyscale.GreyscaleImageFilter
import models.images.GreyscaleImage

class FilterHandler(filter: GreyscaleImageFilter) extends Handler[GreyscaleImage, GreyscaleImage] {

  override def handle(value: GreyscaleImage): GreyscaleImage = {
    val filteredImage = filter.filter(value)
    if (nextHandler.isEmpty)
      filteredImage
    else
      nextHandler.get.handle(filteredImage)
  }

}
