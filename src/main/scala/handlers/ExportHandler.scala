package handlers

import exporters.ascii.ASCIIImageExporter
import models.images.ASCIIImage

class ExportHandler(exporter: ASCIIImageExporter) extends Handler[ASCIIImage, Unit] {

  override def handle(value: ASCIIImage): Unit = {
    exporter.`export`(value)
    if (nextHandler.nonEmpty)
      nextHandler.get.handle(value)
  }
}
