package handlers

trait Handler[I, O] {

  var nextHandler: Option[Handler[I, O]] = None

  def handle(value: I): O

  def setNext(nextHandler: Handler[I, O]): Unit = {
    this.nextHandler = Some(nextHandler)
  }
}

