package handlers

import importers.rgb.RGBImageImporter
import models.images.RGBImage

class ImportHandler(importer: RGBImageImporter) extends Handler[Unit, RGBImage] {

  override def handle(value: Unit): RGBImage = {
    importer.`import`()
  }
}
