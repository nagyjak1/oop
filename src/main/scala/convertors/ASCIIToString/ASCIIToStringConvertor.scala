package convertors.ASCIIToString

import convertors.Convertor
import models.images.ASCIIImage

class ASCIIToStringConvertor extends Convertor[ASCIIImage, String] {
  override def convert(input: ASCIIImage): String = {

    val result = new StringBuilder()

    val height = input.height()
    val width = input.width()

    for (i <- 0 until height) {
      for (j <- 0 until width) {
        result.append( input.pixelGrid().getPixel(j, i).getValue )
      }
      result.append('\n')
    }
    result.toString()
  }
}
