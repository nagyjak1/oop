package convertors.greyscaleToASCII

import models.grids.arrayPixelGrids.ASCIIPixelGrid
import models.images.{ASCIIImage, GreyscaleImage}
import models.pixels.ASCIIPixel

class NonLinearConvertor(table: Array[(Char, Int, Int)]) extends GreyscaleToASCIIConvertor {

  for ( i <- 0 until 256 ) {
    if ( table.count( (x: (Char, Int, Int)) => { x._2 <= i && i <= x._3} ) != 1 ) {
      throw new IllegalArgumentException( "The conversion table is set incorrectly")
    }
  }

  override def convert(input: GreyscaleImage): ASCIIImage = {
    val height = input.height()
    val width = input.width()
    val ASCIPixelGrid = new ASCIIPixelGrid(height, width)

    for (i <- 0 until height) {
      for (j <- 0 until width) {
        val originalPixel = input.pixelGrid().getPixel(j, i).getValue
        val ASCIPixel = new ASCIIPixel(greyscaleToASCII(originalPixel.toChar))
        ASCIPixelGrid.setPixel(j, i, ASCIPixel)
      }
    }
    new ASCIIImage(ASCIPixelGrid)
  }

  private def greyscaleToASCII( greyscale: Int ): Char = {


    val convertRule = table.find( (x: (Char, Int, Int)) => { x._2 <= greyscale && greyscale <= x._3} )

    if ( convertRule.isEmpty )
      throw new IllegalArgumentException( "The conversion table is set incorrectly")
    else
      convertRule.get._1
  }
}
