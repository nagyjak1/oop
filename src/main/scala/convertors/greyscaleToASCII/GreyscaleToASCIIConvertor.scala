package convertors.greyscaleToASCII

import convertors.Convertor
import models.images.{ASCIIImage, GreyscaleImage}

trait GreyscaleToASCIIConvertor extends Convertor[GreyscaleImage, ASCIIImage] {}
