package convertors.greyscaleToASCII

import models.grids.arrayPixelGrids.ASCIIPixelGrid
import models.images.{ASCIIImage, GreyscaleImage}
import models.pixels.ASCIIPixel

class LinearConvertor(conversionTable: String) extends GreyscaleToASCIIConvertor {

  require( conversionTable.nonEmpty )

  override def convert(input: GreyscaleImage): ASCIIImage = {

    val height = input.height()
    val width = input.width()
    val ASCIPixelGrid = new ASCIIPixelGrid(height, width)

    for (i <- 0 until height) {
      for (j <- 0 until width) {
        val originalPixel = input.pixelGrid().getPixel(j, i).getValue
        val ASCIPixel = new ASCIIPixel(greyScaleToASCI(originalPixel))
        ASCIPixelGrid.setPixel(j, i, ASCIPixel)
      }
    }
    new ASCIIImage(ASCIPixelGrid)
  }

  private def greyScaleToASCI(d: Int): Char = {
    val numsToOneChar = 256.0 / conversionTable.length
    conversionTable((d / numsToOneChar).floor.toInt)
  }
}
