package convertors.greyscaleToASCII

import models.images.{ASCIIImage, GreyscaleImage}

class DefaultNonLinearConvertor extends NonLinearConvertor( Array(
    ('*', 0, 50),
    ('.', 51, 132),
    ('-',133,178),
    ('@',179,219),
    ('^',220,230),
    ('!',231,240),
    ('~',241,249),
    ('@',250,255))
) {
  override def convert(input: GreyscaleImage): ASCIIImage = super.convert(input)
}
