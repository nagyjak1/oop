package convertors.greyscaleToASCII

import models.images.{ASCIIImage, GreyscaleImage}

class DefaultLinearConvertor extends LinearConvertor(" .:-=+*#%@") {
  override def convert(input: GreyscaleImage): ASCIIImage = super.convert(input)
}
