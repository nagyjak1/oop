package convertors.RGBToGreyscale

import convertors.Convertor
import models.grids.arrayPixelGrids.GreyscalePixelGrid
import models.images.{GreyscaleImage, RGBImage}
import models.pixels.GreyscalePixel

class RGBToGreyscaleConvertor extends Convertor[RGBImage, GreyscaleImage] {
  override def convert(input: RGBImage): GreyscaleImage = {

    val height = input.height()
    val width = input.width()
    val GreyscalePixelGrid = new GreyscalePixelGrid(height, width)

    for (i <- 0 until height) {
      for (j <- 0 until width) {
        val RGBPixel = input.pixelGrid().getPixel(j, i)
        val GreyscalePixel = new GreyscalePixel(RGBPixel.greyScale)
        GreyscalePixelGrid.setPixel(j, i, GreyscalePixel)
      }
    }
    new GreyscaleImage(GreyscalePixelGrid)
  }
}
