package convertors

trait Convertor[T, M] {
  def convert( input: T ): M
}
