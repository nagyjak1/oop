package importers.rgb.file

import models.images.RGBImage
import org.scalatest.FunSuite

import java.io.File
import javax.imageio.ImageIO

class DefaultFileTypeImporterTest extends FunSuite {

  test("Importer should throw an exception if path does not end with .jpg, .png, or .gif") {
    val wrongPath = "/wrong/path/file.txt"
    assertThrows[IllegalArgumentException] {
      new DefaultFileTypeImporter(wrongPath)
    }
  }

  test("Importer should return an RGBImage when importing .jpg file") {
    val testPath = "src/resources/tests/test-image.jpg"
    val importer = new DefaultFileTypeImporter(testPath)
    val testImage = importer.`import`()
    assert(testImage.isInstanceOf[RGBImage])
  }

  test("Importer should return an RGBImage when importing .png file") {
    val testPath = "src/resources/tests/test-image.png"
    val importer = new DefaultFileTypeImporter(testPath)
    val testImage = importer.`import`()
    assert(testImage.isInstanceOf[RGBImage])
  }

  test("Importer should return an RGBImage when importing .gif file") {
    val testPath = "src/resources/tests/test-image.gif"
    val importer = new DefaultFileTypeImporter(testPath)
    val testImage = importer.`import`()
    assert(testImage.isInstanceOf[RGBImage])
  }

  test("Importer should set the image's height and width to the correct size") {
    val testPath = "src/resources/tests/test-image.png"
    val importer = new DefaultFileTypeImporter(testPath)
    val image = ImageIO.read( new File(testPath) )
    val testImage = importer.`import`()
    val pixelGrid = testImage.pixelGrid()
    assert(testImage.width() == image.getWidth )
    assert(testImage.height() == image.getHeight )
  }


  test ("Importer should correctly import an existing .png image") {

    val fileImporter = new DefaultFileTypeImporter( "src/resources/tests/test-image.png")
    val image: RGBImage = fileImporter.`import`()

    val imageRef = ImageIO.read( new File ("src/resources/tests/test-image.png"))

    assert(image.height() == 3)
    assert(image.width() == 4)
    for ( i <- 0 until image.height() )
      for ( j <- 0 until image.width() )
        assert(image.pixelGrid().getPixel(j, i).getValue == imageRef.getRGB(j,i))
  }

  test ("Importer should correctly import an existing .jpg image") {

    val fileImporter = new DefaultFileTypeImporter( "src/resources/tests/test-image.jpg")
    val image: RGBImage = fileImporter.`import`()

    val imageRef = ImageIO.read( new File ("src/resources/tests/test-image.jpg"))

    assert(image.height() == 3)
    assert(image.width() == 4)
    for ( i <- 0 until image.height() )
      for ( j <- 0 until image.width() )
        assert(image.pixelGrid().getPixel(j, i).getValue == imageRef.getRGB(j,i))
  }

  test ("Importer should correctly import an existing .gif image") {

    val fileImporter = new DefaultFileTypeImporter( "src/resources/tests/test-image.gif")
    val image: RGBImage = fileImporter.`import`()

    val imageRef = ImageIO.read( new File ("src/resources/tests/test-image.gif"))

    assert(image.height() == 3)
    assert(image.width() == 4)
    for ( i <- 0 until image.height() )
      for ( j <- 0 until image.width() )
        assert(image.pixelGrid().getPixel(j, i).getValue == imageRef.getRGB(j,i))
  }
}
