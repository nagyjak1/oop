package importers.rgb

import org.scalatest.FunSuite

import scala.util.Random

class RandomImageImporterTest extends FunSuite {

  test("Generation with same random seed") {

    val randomImageImporter = new RandomImageImporter(1000)
    val randomImage = randomImageImporter.`import`()

    Random.setSeed(1000)
    val height = Random.between(20, 700)
    val width = Random.between(20, 700)

    for (i <- 0 until height) {
      for (j <- 0 until width)
        assert(randomImage.pixelGrid().getPixel(j, i).getValue == Random.between(0xff000000, 0xffffffff))
    }
  }
}
