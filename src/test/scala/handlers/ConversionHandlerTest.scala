package handlers

import convertors.greyscaleToASCII.DefaultLinearConvertor
import models.grids.arrayPixelGrids.{ASCIIPixelGrid, GreyscalePixelGrid}
import models.images.{ASCIIImage, GreyscaleImage}
import models.pixels.{ASCIIPixel, GreyscalePixel}
import org.scalatest.FunSuite

class ConversionHandlerTest extends FunSuite {

  val grid = new GreyscalePixelGrid(3, 3)
  grid.setPixel(0, 0, new GreyscalePixel(0))
  grid.setPixel(1, 0, new GreyscalePixel(31))
  grid.setPixel(2, 0, new GreyscalePixel(32))
  grid.setPixel(0, 1, new GreyscalePixel(63))
  grid.setPixel(1, 1, new GreyscalePixel(159))
  grid.setPixel(2, 1, new GreyscalePixel(191))
  grid.setPixel(0, 2, new GreyscalePixel(193))
  grid.setPixel(1, 2, new GreyscalePixel(223))
  grid.setPixel(2, 2, new GreyscalePixel(255))
  val greyscaleImage = new GreyscaleImage(grid)


  private def ASCIIImagesAreEqual(first: ASCIIImage, second: ASCIIImage): Boolean = {

    if (first.height() != second.height() || first.width() != second.width())
      return false

    for (i <- 0 until first.height())
      for (j <- 0 until first.width())
        if (first.pixelGrid().getPixel(j, i).getValue != second.pixelGrid().getPixel(j, i).getValue)
          return false

    true
  }

  test("ASCIIIImagesAreEqual method test") {
    val pixelGrid = new ASCIIPixelGrid(2, 2)
    pixelGrid.setPixel(0, 0, new ASCIIPixel('a'))
    pixelGrid.setPixel(1, 0, new ASCIIPixel('b'))
    pixelGrid.setPixel(0, 1, new ASCIIPixel('c'))
    pixelGrid.setPixel(1, 1, new ASCIIPixel('d'))
    val image = new ASCIIImage(pixelGrid)
    assert(ASCIIImagesAreEqual(image, image))
  }

  test("ConversionHandler should return ASCIIImage") {
    val convertor = new DefaultLinearConvertor
    val conversionHandler = new ConversionHandler(convertor)
    val result = conversionHandler.handle(greyscaleImage)
    assert(result.isInstanceOf[ASCIIImage])
  }


  test("ConversionHandler should call convert method on the convertor") {
    val convertor = new DefaultLinearConvertor
    val conversionHandler = new ConversionHandler(convertor)
    val result: ASCIIImage = conversionHandler.handle(greyscaleImage)
    assert(ASCIIImagesAreEqual(result, convertor.convert(greyscaleImage)))
  }


}
