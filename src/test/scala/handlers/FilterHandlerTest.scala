package handlers

import filters.greyscale.GreyscaleImageFilter
import filters.greyscale.flip.{FlipFilter, XFlip}
import filters.greyscale.invert.InvertFilter
import filters.greyscale.rotate.RotateFilter
import models.grids.arrayPixelGrids.{ASCIIPixelGrid, GreyscalePixelGrid}
import models.images.{ASCIIImage, GreyscaleImage}
import models.pixels.{ASCIIPixel, GreyscalePixel}
import org.mockito.MockitoSugar.{mock, when}
import org.scalatest.FunSuite

class FilterHandlerTest extends FunSuite {

  private def greyscaleImagesAreEqual( first: GreyscaleImage, second: GreyscaleImage ): Boolean = {

    if ( first.height() != second.height() || first.width() != second.width() )
      return false

    for ( i <- 0 until first.height() )
      for ( j <- 0 until first.width() )
        if ( first.pixelGrid().getPixel(j, i).getValue != second.pixelGrid().getPixel(j,i).getValue )
          return false

    true
  }

  test( "greyscaleImagesAreEqual method test" ) {
    val pixelGrid = new GreyscalePixelGrid(2,2)
    pixelGrid.setPixel(0,0, new GreyscalePixel(10))
    pixelGrid.setPixel(1,0, new GreyscalePixel(50))
    pixelGrid.setPixel(0,1, new GreyscalePixel(120))
    pixelGrid.setPixel(1,1, new GreyscalePixel(220))
    val image = new GreyscaleImage( pixelGrid )
    assert( greyscaleImagesAreEqual( image, image) )
  }

  val greyscalePixelGrid = new GreyscalePixelGrid(2,2)
  greyscalePixelGrid.setPixel(0,0, new GreyscalePixel(20))
  greyscalePixelGrid.setPixel(1,0, new GreyscalePixel(50))
  greyscalePixelGrid.setPixel(0,1, new GreyscalePixel(100))
  greyscalePixelGrid.setPixel(1,1, new GreyscalePixel(200))
  val greyscaleImage = new GreyscaleImage( greyscalePixelGrid )

  val filter: GreyscaleImageFilter = mock[GreyscaleImageFilter]
  val filterHandler = new FilterHandler(filter)

  test("FilterHandler should return an GreyscaleImage") {
    val filter = new InvertFilter()
    val handler = new FilterHandler(filter)
    val result = handler.handle( greyscaleImage )
    assert(result.isInstanceOf[GreyscaleImage])
  }

  test("FilterHandler should call the filter method on the Filter") {
    val filter = new InvertFilter()
    val handler = new FilterHandler(filter)
    assert ( greyscaleImagesAreEqual( filter.filter( greyscaleImage), handler.handle(greyscaleImage) ))
  }

  test("Two FilterHandlers in chain") {
    val invertFilter = new InvertFilter()
    val flipFilter = new FlipFilter( XFlip() )
    val firstHandler = new FilterHandler( invertFilter)
    val secondHandler = new FilterHandler( flipFilter)
    firstHandler.setNext(secondHandler)
    assert ( greyscaleImagesAreEqual( flipFilter.filter( invertFilter.filter(greyscaleImage)), firstHandler.handle(greyscaleImage) ))
  }

}