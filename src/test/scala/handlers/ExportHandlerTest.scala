package handlers

import exporters.ascii.StreamExporter
import models.grids.arrayPixelGrids.ASCIIPixelGrid
import models.images.ASCIIImage
import models.pixels.ASCIIPixel
import org.scalatest.FunSuite

import java.io.ByteArrayOutputStream

class ExportHandlerTest extends FunSuite {

  val rectangleASCIIPixelGrid = new ASCIIPixelGrid(2, 3)
  rectangleASCIIPixelGrid.setPixel(0, 0, new ASCIIPixel('a'))
  rectangleASCIIPixelGrid.setPixel(1, 0, new ASCIIPixel('b'))
  rectangleASCIIPixelGrid.setPixel(2, 0, new ASCIIPixel('c'))
  rectangleASCIIPixelGrid.setPixel(0, 1, new ASCIIPixel('d'))
  rectangleASCIIPixelGrid.setPixel(1, 1, new ASCIIPixel('e'))
  rectangleASCIIPixelGrid.setPixel(2, 1, new ASCIIPixel('f'))
  val rectangleASCIIImage = new ASCIIImage(rectangleASCIIPixelGrid)


  test("ExportHandler should call the export method on the Exporter") {

    val stream = new ByteArrayOutputStream()
    val exporter = new StreamExporter(stream)

    val handler = new ExportHandler(exporter)

    handler.handle(rectangleASCIIImage)

    assert(stream.toString() == "abc\ndef\n")
  }

  test("Two ExportHandlers in chain") {

    val firstStream = new ByteArrayOutputStream()
    val secondStream = new ByteArrayOutputStream()
    val firstExporter = new StreamExporter( firstStream )
    val secondExporter = new StreamExporter( secondStream )
    val firstHandler = new ExportHandler( firstExporter)
    val secondHandler = new ExportHandler( secondExporter )
    firstHandler.setNext(secondHandler)

    firstHandler.handle( rectangleASCIIImage )

    assert ( firstStream.toString() == "abc\ndef\n" )
    assert ( secondStream.toString() == "abc\ndef\n" )
  }

}
