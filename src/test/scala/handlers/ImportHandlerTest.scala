package handlers

import importers.rgb.RGBImageImporter
import importers.rgb.file.DefaultFileTypeImporter
import models.grids.arrayPixelGrids.{ASCIIPixelGrid, RGBPixelGrid}
import models.images.{ASCIIImage, RGBImage}
import models.pixels.{ASCIIPixel, RGBPixel}
import org.scalatest.FunSuite

class ImportHandlerTest extends FunSuite {

  private def RGBImagesAreEqual( first: RGBImage, second: RGBImage ): Boolean = {

    if ( first.height() != second.height() || first.width() != second.width() )
      return false

    for ( i <- 0 until first.height() )
      for ( j <- 0 until first.width() )
        if ( first.pixelGrid().getPixel(j, i).getValue != second.pixelGrid().getPixel(j,i).getValue )
          return false

    true
  }

  test( "RGBImagesAreEqual method test" ) {
    val pixelGrid = new RGBPixelGrid(2,2)
    pixelGrid.setPixel(1,0, new RGBPixel(0xff121212))
    pixelGrid.setPixel(0,0, new RGBPixel(0xffffffff))
    pixelGrid.setPixel(0,1, new RGBPixel(0xff111111))
    pixelGrid.setPixel(1,1, new RGBPixel(0xff000110))
    val image = new RGBImage( pixelGrid )
    assert( RGBImagesAreEqual( image, image) )
  }

  test("ImportHandler should return an RGBImage") {
    val importer = new DefaultFileTypeImporter("src/resources/tests/test-image.gif")
    val handler = new ImportHandler(importer)
    val result = handler.handle(())
    assert(result.isInstanceOf[RGBImage])
  }

  test("ImportHandler should call the 'import' method on the RGBImageImporter") {
    val importer = new DefaultFileTypeImporter("src/resources/tests/test-image.gif")
    val handler = new ImportHandler(importer)
    assert ( RGBImagesAreEqual( importer.`import`(), handler.handle() ))
  }
}
