package filters.greyscale.brightness

import models.grids.arrayPixelGrids.GreyscalePixelGrid
import models.images.GreyscaleImage
import models.pixels.GreyscalePixel
import org.scalatest.FunSuite

class BrightnessFilterTest extends FunSuite {

  /*
  111 123 142 091
  005 131 001 241
  255 010 039 211 */
  val rectangleGrid = new GreyscalePixelGrid(3, 4)
  val rectangleImage = new GreyscaleImage(rectangleGrid)
  rectangleGrid.setPixel(0, 0, new GreyscalePixel(111))
  rectangleGrid.setPixel(1, 0, new GreyscalePixel(123))
  rectangleGrid.setPixel(2, 0, new GreyscalePixel(142))
  rectangleGrid.setPixel(3, 0, new GreyscalePixel(91))
  rectangleGrid.setPixel(0, 1, new GreyscalePixel(5))
  rectangleGrid.setPixel(1, 1, new GreyscalePixel(131))
  rectangleGrid.setPixel(2, 1, new GreyscalePixel(1))
  rectangleGrid.setPixel(3, 1, new GreyscalePixel(241))
  rectangleGrid.setPixel(0, 2, new GreyscalePixel(255))
  rectangleGrid.setPixel(1, 2, new GreyscalePixel(10))
  rectangleGrid.setPixel(2, 2, new GreyscalePixel(39))
  rectangleGrid.setPixel(3, 2, new GreyscalePixel(211))

  def roundGreyscale(value: Int): Int = {
    if (value < 0)
      0
    else if (value > 255)
      255
    else
      value
  }

  test("Brighten all pixels over 255") {
    val brightnessFilter = new BrightnessFilter(1000)
    val brightenedImage = brightnessFilter.filter(rectangleImage)

    assert(brightenedImage.height() == 3)
    assert(brightenedImage.width() == 4)

    for (i <- 0 until brightenedImage.height()) {
      for (j <- 0 until brightenedImage.width())
        assert(brightenedImage.pixelGrid().getPixel(j, i).getValue == 255)
    }
  }

  test("Brighten all pixels below 0") {
    val brightnessFilter = new BrightnessFilter(-1000)
    val brightenedImage = brightnessFilter.filter(rectangleImage)

    assert(brightenedImage.height() == 3)
    assert(brightenedImage.width() == 4)

    for (i <- 0 until brightenedImage.height()) {
      for (j <- 0 until brightenedImage.width())
        assert(brightenedImage.pixelGrid().getPixel(j, i).getValue == 0)
    }
  }

  test("Brighten +0") {
    val brightnessFilter = new BrightnessFilter(0)
    val brightenedImage = brightnessFilter.filter(rectangleImage)

    assert(brightenedImage.height() == 3)
    assert(brightenedImage.width() == 4)

    for (i <- 0 until brightenedImage.height()) {
      for (j <- 0 until brightenedImage.width())
        assert(brightenedImage.pixelGrid().getPixel(j, i).getValue == rectangleImage.pixelGrid().getPixel(j, i).getValue)
    }
  }

  test("Brighten +100 rectangle image") {
    val brightnessFilter = new BrightnessFilter(+100)
    val brightenedImage = brightnessFilter.filter(rectangleImage)

    assert(brightenedImage.height() == 3)
    assert(brightenedImage.width() == 4)

    for (i <- 0 until brightenedImage.height()) {
      for (j <- 0 until brightenedImage.width()) {
        val originalValue = rectangleImage.pixelGrid().getPixel(j, i).getValue
        val newValue = roundGreyscale(originalValue + 100)
        assert(brightenedImage.pixelGrid().getPixel(j, i).getValue == newValue)
      }
    }
  }

  test("Brighten -50 rectangle image") {
    val brightnessFilter = new BrightnessFilter(-50)
    val brightenedImage = brightnessFilter.filter(rectangleImage)

    assert(brightenedImage.height() == 3)
    assert(brightenedImage.width() == 4)

    for (i <- 0 until brightenedImage.height()) {
      for (j <- 0 until brightenedImage.width()) {
        val originalValue = rectangleImage.pixelGrid().getPixel(j, i).getValue
        val newValue = roundGreyscale(originalValue - 50)
        assert(brightenedImage.pixelGrid().getPixel(j, i).getValue == newValue)
      }
    }
  }

}
