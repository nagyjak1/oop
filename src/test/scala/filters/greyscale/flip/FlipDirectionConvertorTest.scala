package filters.greyscale.flip

import org.scalatest.FunSuite

class FlipDirectionConvertorTest extends FunSuite {

  val convertor: FlipDirectionConvertor.type = FlipDirectionConvertor

  test("Flip x test") {
    val result = convertor.convert("x")
    assert ( result.isInstanceOf[XFlip] )
  }

  test("Flip y test") {
    val result = convertor.convert("y")
    assert ( result.isInstanceOf[YFlip] )
  }

  test("Flip wrong argument test") {
    assertThrows[IllegalArgumentException] ( convertor.convert("z") )
  }
}
