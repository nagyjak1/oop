package filters.greyscale.flip

import models.grids.arrayPixelGrids.GreyscalePixelGrid
import models.images.GreyscaleImage
import models.pixels.GreyscalePixel
import org.scalatest.FunSuite

class FlipFilterTest extends FunSuite {
  /*
  7 8 9
  4 5 6
  1 2 3 */
  val squareGrid = new GreyscalePixelGrid(3, 3)
  squareGrid.setPixel(0, 0, new GreyscalePixel(7))
  squareGrid.setPixel(1, 0, new GreyscalePixel(8))
  squareGrid.setPixel(2, 0, new GreyscalePixel(9))
  squareGrid.setPixel(0, 1, new GreyscalePixel(4))
  squareGrid.setPixel(1, 1, new GreyscalePixel(5))
  squareGrid.setPixel(2, 1, new GreyscalePixel(6))
  squareGrid.setPixel(0, 2, new GreyscalePixel(1))
  squareGrid.setPixel(1, 2, new GreyscalePixel(2))
  squareGrid.setPixel(2, 2, new GreyscalePixel(3))
  val squareImage = new GreyscaleImage(squareGrid)

  /*
  09 10 11 12
  05 06 07 08
  01 02 03 04 */
  val verticalRectangleGrid = new GreyscalePixelGrid(3, 4)
  verticalRectangleGrid.setPixel(0, 0, new GreyscalePixel(9))
  verticalRectangleGrid.setPixel(1, 0, new GreyscalePixel(10))
  verticalRectangleGrid.setPixel(2, 0, new GreyscalePixel(11))
  verticalRectangleGrid.setPixel(3, 0, new GreyscalePixel(12))
  verticalRectangleGrid.setPixel(0, 1, new GreyscalePixel(5))
  verticalRectangleGrid.setPixel(1, 1, new GreyscalePixel(6))
  verticalRectangleGrid.setPixel(2, 1, new GreyscalePixel(7))
  verticalRectangleGrid.setPixel(3, 1, new GreyscalePixel(8))
  verticalRectangleGrid.setPixel(0, 2, new GreyscalePixel(1))
  verticalRectangleGrid.setPixel(1, 2, new GreyscalePixel(2))
  verticalRectangleGrid.setPixel(2, 2, new GreyscalePixel(3))
  verticalRectangleGrid.setPixel(3, 2, new GreyscalePixel(4))
  val verticalRectangleImage = new GreyscaleImage(verticalRectangleGrid)

  /*
  10 11 12
  07 08 09
  04 05 06
  01 02 03 */
  val horizontalRectangleGrid = new GreyscalePixelGrid(4, 3)
  horizontalRectangleGrid.setPixel(0, 0, new GreyscalePixel(10))
  horizontalRectangleGrid.setPixel(1, 0, new GreyscalePixel(11))
  horizontalRectangleGrid.setPixel(2, 0, new GreyscalePixel(12))
  horizontalRectangleGrid.setPixel(0, 1, new GreyscalePixel(7))
  horizontalRectangleGrid.setPixel(1, 1, new GreyscalePixel(8))
  horizontalRectangleGrid.setPixel(2, 1, new GreyscalePixel(9))
  horizontalRectangleGrid.setPixel(0, 2, new GreyscalePixel(4))
  horizontalRectangleGrid.setPixel(1, 2, new GreyscalePixel(5))
  horizontalRectangleGrid.setPixel(2, 2, new GreyscalePixel(6))
  horizontalRectangleGrid.setPixel(0, 3, new GreyscalePixel(1))
  horizontalRectangleGrid.setPixel(1, 3, new GreyscalePixel(2))
  horizontalRectangleGrid.setPixel(2, 3, new GreyscalePixel(3))
  val horizontalRectangleImage = new GreyscaleImage(horizontalRectangleGrid)

  test("Flip x-axis horizontal rectangle image") {
    val flipFilter = new FlipFilter(XFlip())
    val flippedImage = flipFilter.filter(verticalRectangleImage)

    /*
    09 10 11 12      01 02 03 04
    05 06 07 08  ->  05 06 07 08
    01 02 03 04      09 10 11 12 */
    assert(flippedImage.height() == 3)
    assert(flippedImage.width() == 4)
    assert(flippedImage.pixelGrid().getPixel(0, 0).getValue == 1)
    assert(flippedImage.pixelGrid().getPixel(1, 0).getValue == 2)
    assert(flippedImage.pixelGrid().getPixel(2, 0).getValue == 3)
    assert(flippedImage.pixelGrid().getPixel(3, 0).getValue == 4)
    assert(flippedImage.pixelGrid().getPixel(0, 1).getValue == 5)
    assert(flippedImage.pixelGrid().getPixel(1, 1).getValue == 6)
    assert(flippedImage.pixelGrid().getPixel(2, 1).getValue == 7)
    assert(flippedImage.pixelGrid().getPixel(3, 1).getValue == 8)
    assert(flippedImage.pixelGrid().getPixel(0, 2).getValue == 9)
    assert(flippedImage.pixelGrid().getPixel(1, 2).getValue == 10)
    assert(flippedImage.pixelGrid().getPixel(2, 2).getValue == 11)
    assert(flippedImage.pixelGrid().getPixel(3, 2).getValue == 12)
  }

  test("Flip y-axis horizontal rectangle image") {
    val flipFilter = new FlipFilter(YFlip())
    val flippedImage = flipFilter.filter(verticalRectangleImage)

    /*
    09 10 11 12      12 11 10 09
    05 06 07 08  ->  08 07 06 05
    01 02 03 04      04 03 02 01 */
    assert(flippedImage.height() == 3)
    assert(flippedImage.width() == 4)
    assert(flippedImage.pixelGrid().getPixel(0, 0).getValue == 12)
    assert(flippedImage.pixelGrid().getPixel(1, 0).getValue == 11)
    assert(flippedImage.pixelGrid().getPixel(2, 0).getValue == 10)
    assert(flippedImage.pixelGrid().getPixel(3, 0).getValue == 9)
    assert(flippedImage.pixelGrid().getPixel(0, 1).getValue == 8)
    assert(flippedImage.pixelGrid().getPixel(1, 1).getValue == 7)
    assert(flippedImage.pixelGrid().getPixel(2, 1).getValue == 6)
    assert(flippedImage.pixelGrid().getPixel(3, 1).getValue == 5)
    assert(flippedImage.pixelGrid().getPixel(0, 2).getValue == 4)
    assert(flippedImage.pixelGrid().getPixel(1, 2).getValue == 3)
    assert(flippedImage.pixelGrid().getPixel(2, 2).getValue == 2)
    assert(flippedImage.pixelGrid().getPixel(3, 2).getValue == 1)
  }

  test("Flip x-axis vertical rectangle image") {
    val flipFilter = new FlipFilter(XFlip())
    val flippedImage = flipFilter.filter(horizontalRectangleImage)

    /*
    10 11 12      01 02 03
    07 08 09  ->  04 05 06
    04 05 06      07 08 09
    01 02 03      10 11 12 */
    assert(flippedImage.height() == 4)
    assert(flippedImage.width() == 3)
    assert(flippedImage.pixelGrid().getPixel(0, 0).getValue == 1)
    assert(flippedImage.pixelGrid().getPixel(1, 0).getValue == 2)
    assert(flippedImage.pixelGrid().getPixel(2, 0).getValue == 3)
    assert(flippedImage.pixelGrid().getPixel(0, 1).getValue == 4)
    assert(flippedImage.pixelGrid().getPixel(1, 1).getValue == 5)
    assert(flippedImage.pixelGrid().getPixel(2, 1).getValue == 6)
    assert(flippedImage.pixelGrid().getPixel(0, 2).getValue == 7)
    assert(flippedImage.pixelGrid().getPixel(1, 2).getValue == 8)
    assert(flippedImage.pixelGrid().getPixel(2, 2).getValue == 9)
    assert(flippedImage.pixelGrid().getPixel(0, 3).getValue == 10)
    assert(flippedImage.pixelGrid().getPixel(1, 3).getValue == 11)
    assert(flippedImage.pixelGrid().getPixel(2, 3).getValue == 12)
  }

  test("Flip y-axis vertical rectangle image") {
    val flipFilter = new FlipFilter(YFlip())
    val flippedImage = flipFilter.filter(horizontalRectangleImage)

    /*
    10 11 12      12 11 10
    07 08 09  ->  09 08 07
    04 05 06      06 05 04
    01 02 03      03 02 01 */
    assert(flippedImage.height() == 4)
    assert(flippedImage.width() == 3)
    assert(flippedImage.pixelGrid().getPixel(0, 0).getValue == 12)
    assert(flippedImage.pixelGrid().getPixel(1, 0).getValue == 11)
    assert(flippedImage.pixelGrid().getPixel(2, 0).getValue == 10)
    assert(flippedImage.pixelGrid().getPixel(0, 1).getValue == 9)
    assert(flippedImage.pixelGrid().getPixel(1, 1).getValue == 8)
    assert(flippedImage.pixelGrid().getPixel(2, 1).getValue == 7)
    assert(flippedImage.pixelGrid().getPixel(0, 2).getValue == 6)
    assert(flippedImage.pixelGrid().getPixel(1, 2).getValue == 5)
    assert(flippedImage.pixelGrid().getPixel(2, 2).getValue == 4)
    assert(flippedImage.pixelGrid().getPixel(0, 3).getValue == 3)
    assert(flippedImage.pixelGrid().getPixel(1, 3).getValue == 2)
    assert(flippedImage.pixelGrid().getPixel(2, 3).getValue == 1)
  }

  test("Flip x-axis square image") {
    val flipFilter = new FlipFilter(XFlip())
    val flippedImage = flipFilter.filter(squareImage)

    /*
    7 8 9      1 2 3
    4 5 6  ->  4 5 6
    1 2 3      7 8 9 */
    assert(flippedImage.height() == 3)
    assert(flippedImage.width() == 3)
    assert(flippedImage.pixelGrid().getPixel(0, 0).getValue == 1)
    assert(flippedImage.pixelGrid().getPixel(1, 0).getValue == 2)
    assert(flippedImage.pixelGrid().getPixel(2, 0).getValue == 3)
    assert(flippedImage.pixelGrid().getPixel(0, 1).getValue == 4)
    assert(flippedImage.pixelGrid().getPixel(1, 1).getValue == 5)
    assert(flippedImage.pixelGrid().getPixel(2, 1).getValue == 6)
    assert(flippedImage.pixelGrid().getPixel(0, 2).getValue == 7)
    assert(flippedImage.pixelGrid().getPixel(1, 2).getValue == 8)
    assert(flippedImage.pixelGrid().getPixel(2, 2).getValue == 9)
  }

  test("Flip y-axis square image") {
    val flipFilter = new FlipFilter(YFlip())
    val flippedImage = flipFilter.filter(squareImage)

    /*
    7 8 9      9 8 7
    4 5 6  ->  6 5 4
    1 2 3      3 2 1 */
    assert(flippedImage.height() == 3)
    assert(flippedImage.width() == 3)
    assert(flippedImage.pixelGrid().getPixel(0, 0).getValue == 9)
    assert(flippedImage.pixelGrid().getPixel(1, 0).getValue == 8)
    assert(flippedImage.pixelGrid().getPixel(2, 0).getValue == 7)
    assert(flippedImage.pixelGrid().getPixel(0, 1).getValue == 6)
    assert(flippedImage.pixelGrid().getPixel(1, 1).getValue == 5)
    assert(flippedImage.pixelGrid().getPixel(2, 1).getValue == 4)
    assert(flippedImage.pixelGrid().getPixel(0, 2).getValue == 3)
    assert(flippedImage.pixelGrid().getPixel(1, 2).getValue == 2)
    assert(flippedImage.pixelGrid().getPixel(2, 2).getValue == 1)
  }
}
