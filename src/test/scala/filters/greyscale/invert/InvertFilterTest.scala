package filters.greyscale.invert

import models.grids.arrayPixelGrids.GreyscalePixelGrid
import models.images.GreyscaleImage
import models.pixels.GreyscalePixel
import org.scalatest.FunSuite

class InvertFilterTest extends FunSuite {

  /*
  121 110 031
  031 132 132
  100 255 001 */
  val squareGrid = new GreyscalePixelGrid(3, 3)
  squareGrid.setPixel(0, 0, new GreyscalePixel(121))
  squareGrid.setPixel(1, 0, new GreyscalePixel(110))
  squareGrid.setPixel(2, 0, new GreyscalePixel(31))
  squareGrid.setPixel(0, 1, new GreyscalePixel(31))
  squareGrid.setPixel(1, 1, new GreyscalePixel(132))
  squareGrid.setPixel(2, 1, new GreyscalePixel(132))
  squareGrid.setPixel(0, 2, new GreyscalePixel(100))
  squareGrid.setPixel(1, 2, new GreyscalePixel(255))
  squareGrid.setPixel(2, 2, new GreyscalePixel(1))
  val squareImage = new GreyscaleImage(squareGrid)

  /*
  005 121 132 230
  000 240 230 010
  255 000 120 060 */
  val rectangleGrid = new GreyscalePixelGrid(3, 4)
  rectangleGrid.setPixel(0, 0, new GreyscalePixel(5))
  rectangleGrid.setPixel(1, 0, new GreyscalePixel(121))
  rectangleGrid.setPixel(2, 0, new GreyscalePixel(132))
  rectangleGrid.setPixel(3, 0, new GreyscalePixel(230))
  rectangleGrid.setPixel(0, 1, new GreyscalePixel(0))
  rectangleGrid.setPixel(1, 1, new GreyscalePixel(240))
  rectangleGrid.setPixel(2, 1, new GreyscalePixel(230))
  rectangleGrid.setPixel(3, 1, new GreyscalePixel(10))
  rectangleGrid.setPixel(0, 2, new GreyscalePixel(255))
  rectangleGrid.setPixel(1, 2, new GreyscalePixel(0))
  rectangleGrid.setPixel(2, 2, new GreyscalePixel(120))
  rectangleGrid.setPixel(3, 2, new GreyscalePixel(60))
  val rectangleImage = new GreyscaleImage(rectangleGrid)

  test("Invert square image") {
    val invertFilter = new InvertFilter()
    val invertedImage = invertFilter.filter(squareImage)

    /*
    121 110 031
    031 132 132  ->
    100 255 001      */
    assert(invertedImage.height() == 3)
    assert(invertedImage.width() == 3)
    assert(invertedImage.pixelGrid().getPixel(0, 0).getValue == 255-121)
    assert(invertedImage.pixelGrid().getPixel(1, 0).getValue == 255-110)
    assert(invertedImage.pixelGrid().getPixel(2, 0).getValue == 255-31)
    assert(invertedImage.pixelGrid().getPixel(0, 1).getValue == 255-31)
    assert(invertedImage.pixelGrid().getPixel(1, 1).getValue == 255-132)
    assert(invertedImage.pixelGrid().getPixel(2, 1).getValue == 255-132)
    assert(invertedImage.pixelGrid().getPixel(0, 2).getValue == 255-100)
    assert(invertedImage.pixelGrid().getPixel(1, 2).getValue == 255-255)
    assert(invertedImage.pixelGrid().getPixel(2, 2).getValue == 255-1)
  }

  test("Invert rectangle image") {
    val invertFilter = new InvertFilter()
    val invertedImage = invertFilter.filter(rectangleImage)

    /*
    005 121 132 230
    000 240 230 010  ->
    255 000 120 060      */
    assert(invertedImage.height() == 3)
    assert(invertedImage.width() == 4)
    assert(invertedImage.pixelGrid().getPixel(0, 0).getValue == 255-5)
    assert(invertedImage.pixelGrid().getPixel(1, 0).getValue == 255-121)
    assert(invertedImage.pixelGrid().getPixel(2, 0).getValue == 255-132)
    assert(invertedImage.pixelGrid().getPixel(3, 0).getValue == 255-230)
    assert(invertedImage.pixelGrid().getPixel(0, 1).getValue == 255-0)
    assert(invertedImage.pixelGrid().getPixel(1, 1).getValue == 255-240)
    assert(invertedImage.pixelGrid().getPixel(2, 1).getValue == 255-230)
    assert(invertedImage.pixelGrid().getPixel(3, 1).getValue == 255-10)
    assert(invertedImage.pixelGrid().getPixel(0, 2).getValue == 255-255)
    assert(invertedImage.pixelGrid().getPixel(1, 2).getValue == 255-0)
    assert(invertedImage.pixelGrid().getPixel(2, 2).getValue == 255-120)
    assert(invertedImage.pixelGrid().getPixel(3, 2).getValue == 255-60)
  }
}
