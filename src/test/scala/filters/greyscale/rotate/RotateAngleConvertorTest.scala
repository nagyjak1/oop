package filters.greyscale.rotate

import org.scalatest.FunSuite

class RotateAngleConvertorTest extends FunSuite {

  val convertor: RotateAngleConvertor.type = RotateAngleConvertor

  test("Convert +0 test") {
    val result = convertor.convert(+0)
    assert ( result.isInstanceOf[RotateAngle0] )
  }

  test("Convert +90 test") {
    val result = convertor.convert(+90)
    assert ( result.isInstanceOf[RotateAngle90] )
  }

  test("Convert +180 test") {
    val result = convertor.convert(+180)
    assert ( result.isInstanceOf[RotateAngle180] )
  }

  test("Convert +270 test") {
    val result = convertor.convert(+270)
    assert ( result.isInstanceOf[RotateAngle270] )
  }

  test("Convert +360 test") {
    val result = convertor.convert(+360)
    assert ( result.isInstanceOf[RotateAngle0] )
  }

  test("Convert -0 test") {
    val result = convertor.convert(-0)
    assert ( result.isInstanceOf[RotateAngle0] )
  }

  test("Convert -90 test") {
    val result = convertor.convert(-90)
    assert ( result.isInstanceOf[RotateAngle270] )
  }

  test("Convert -180 test") {
    val result = convertor.convert(-180)
    assert ( result.isInstanceOf[RotateAngle180] )
  }

  test("Convert -270 test") {
    val result = convertor.convert(-270)
    assert ( result.isInstanceOf[RotateAngle90] )
  }

  test("Convert -360 test") {
    val result = convertor.convert(-360)
    assert ( result.isInstanceOf[RotateAngle0] )
  }

  test("Convert +810 test") {
    val result = convertor.convert(+810)
    assert ( result.isInstanceOf[RotateAngle90] )
  }

  test("Convert ( 10*(-360)+180 ) test") {
    val result = convertor.convert(10*(-360)+180)
    assert ( result.isInstanceOf[RotateAngle180] )
  }

  test("Convert invalid rotate angle +91") {
    assertThrows[IllegalArgumentException] (convertor.convert(+91))
  }

  test("Convert invalid rotate angle -181") {
    assertThrows[IllegalArgumentException] (convertor.convert(-181))
  }


}
