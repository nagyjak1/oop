package filters.greyscale.rotate

import models.grids.arrayPixelGrids.GreyscalePixelGrid
import models.images.GreyscaleImage
import models.pixels.GreyscalePixel
import org.scalatest.FunSuite

class RotateFilterTest extends FunSuite {

  /*
  7 8 9
  4 5 6
  1 2 3 */
  val squareGrid = new GreyscalePixelGrid(3, 3)
  squareGrid.setPixel(0, 0, new GreyscalePixel(7))
  squareGrid.setPixel(1, 0, new GreyscalePixel(8))
  squareGrid.setPixel(2, 0, new GreyscalePixel(9))
  squareGrid.setPixel(0, 1, new GreyscalePixel(4))
  squareGrid.setPixel(1, 1, new GreyscalePixel(5))
  squareGrid.setPixel(2, 1, new GreyscalePixel(6))
  squareGrid.setPixel(0, 2, new GreyscalePixel(1))
  squareGrid.setPixel(1, 2, new GreyscalePixel(2))
  squareGrid.setPixel(2, 2, new GreyscalePixel(3))
  val squareImage = new GreyscaleImage(squareGrid)

  /*
  09 10 11 12
  05 06 07 08
  01 02 03 04 */
  val rectangleGrid = new GreyscalePixelGrid(3, 4)
  rectangleGrid.setPixel(0, 0, new GreyscalePixel(9))
  rectangleGrid.setPixel(1, 0, new GreyscalePixel(10))
  rectangleGrid.setPixel(2, 0, new GreyscalePixel(11))
  rectangleGrid.setPixel(3, 0, new GreyscalePixel(12))
  rectangleGrid.setPixel(0, 1, new GreyscalePixel(5))
  rectangleGrid.setPixel(1, 1, new GreyscalePixel(6))
  rectangleGrid.setPixel(2, 1, new GreyscalePixel(7))
  rectangleGrid.setPixel(3, 1, new GreyscalePixel(8))
  rectangleGrid.setPixel(0, 2, new GreyscalePixel(1))
  rectangleGrid.setPixel(1, 2, new GreyscalePixel(2))
  rectangleGrid.setPixel(2, 2, new GreyscalePixel(3))
  rectangleGrid.setPixel(3, 2, new GreyscalePixel(4))
  val rectangleImage = new GreyscaleImage(rectangleGrid)


  test("Rotate square image +0") {
    val rotateFilter = new RotateFilter(RotateAngle0())
    val rotatedImage = rotateFilter.filter(squareImage)

    /*
    7 8 9      7 8 9
    4 5 6  ->  4 5 6
    1 2 3      1 2 3 */
    assert(rotatedImage.height() == 3)
    assert(rotatedImage.width() == 3)
    assert(rotatedImage.pixelGrid().getPixel(0, 0).getValue == 7)
    assert(rotatedImage.pixelGrid().getPixel(1, 0).getValue == 8)
    assert(rotatedImage.pixelGrid().getPixel(2, 0).getValue == 9)
    assert(rotatedImage.pixelGrid().getPixel(0, 1).getValue == 4)
    assert(rotatedImage.pixelGrid().getPixel(1, 1).getValue == 5)
    assert(rotatedImage.pixelGrid().getPixel(2, 1).getValue == 6)
    assert(rotatedImage.pixelGrid().getPixel(0, 2).getValue == 1)
    assert(rotatedImage.pixelGrid().getPixel(1, 2).getValue == 2)
    assert(rotatedImage.pixelGrid().getPixel(2, 2).getValue == 3)
  }

  test("Rotate square image +90") {
    val rotateFilter = new RotateFilter(RotateAngle90())
    val rotatedImage = rotateFilter.filter(squareImage)

    /*
    7 8 9      1 4 7
    4 5 6  ->  2 5 8
    1 2 3      3 6 9 */
    assert(rotatedImage.height() == 3)
    assert(rotatedImage.width() == 3)
    assert(rotatedImage.pixelGrid().getPixel(0, 0).getValue == 1)
    assert(rotatedImage.pixelGrid().getPixel(1, 0).getValue == 4)
    assert(rotatedImage.pixelGrid().getPixel(2, 0).getValue == 7)
    assert(rotatedImage.pixelGrid().getPixel(0, 1).getValue == 2)
    assert(rotatedImage.pixelGrid().getPixel(1, 1).getValue == 5)
    assert(rotatedImage.pixelGrid().getPixel(2, 1).getValue == 8)
    assert(rotatedImage.pixelGrid().getPixel(0, 2).getValue == 3)
    assert(rotatedImage.pixelGrid().getPixel(1, 2).getValue == 6)
    assert(rotatedImage.pixelGrid().getPixel(2, 2).getValue == 9)
  }

  test("Rotate square image +180") {
    val rotateFilter = new RotateFilter(RotateAngle180())
    val rotatedImage = rotateFilter.filter(squareImage)

    /*
    7 8 9      3 2 1
    4 5 6  ->  6 5 4
    1 2 3      9 8 7 */
    assert(rotatedImage.height() == 3)
    assert(rotatedImage.width() == 3)
    assert(rotatedImage.pixelGrid().getPixel(0, 0).getValue == 3)
    assert(rotatedImage.pixelGrid().getPixel(1, 0).getValue == 2)
    assert(rotatedImage.pixelGrid().getPixel(2, 0).getValue == 1)
    assert(rotatedImage.pixelGrid().getPixel(0, 1).getValue == 6)
    assert(rotatedImage.pixelGrid().getPixel(1, 1).getValue == 5)
    assert(rotatedImage.pixelGrid().getPixel(2, 1).getValue == 4)
    assert(rotatedImage.pixelGrid().getPixel(0, 2).getValue == 9)
    assert(rotatedImage.pixelGrid().getPixel(1, 2).getValue == 8)
    assert(rotatedImage.pixelGrid().getPixel(2, 2).getValue == 7)
  }

  test("Rotate square image +270") {
    val rotateFilter = new RotateFilter(RotateAngle270())
    val rotatedImage = rotateFilter.filter(squareImage)

    /*
    7 8 9      9 6 3
    4 5 6  ->  8 5 2
    1 2 3      7 4 1 */
    assert(rotatedImage.height() == 3)
    assert(rotatedImage.width() == 3)
    assert(rotatedImage.pixelGrid().getPixel(0, 0).getValue == 9)
    assert(rotatedImage.pixelGrid().getPixel(1, 0).getValue == 6)
    assert(rotatedImage.pixelGrid().getPixel(2, 0).getValue == 3)
    assert(rotatedImage.pixelGrid().getPixel(0, 1).getValue == 8)
    assert(rotatedImage.pixelGrid().getPixel(1, 1).getValue == 5)
    assert(rotatedImage.pixelGrid().getPixel(2, 1).getValue == 2)
    assert(rotatedImage.pixelGrid().getPixel(0, 2).getValue == 7)
    assert(rotatedImage.pixelGrid().getPixel(1, 2).getValue == 4)
    assert(rotatedImage.pixelGrid().getPixel(2, 2).getValue == 1)
  }


  test("Rotate rectangle image +0") {
    val rotateFilter = new RotateFilter(RotateAngle0())
    val rotatedImage = rotateFilter.filter(rectangleImage)

    /*
    09 10 11 12      09 10 11 12
    05 06 07 08  ->  05 06 07 08
    01 02 03 04      01 02 03 04 */
    assert(rotatedImage.height() == 3)
    assert(rotatedImage.width() == 4)
    assert(rotatedImage.pixelGrid().getPixel(0, 0).getValue == 9)
    assert(rotatedImage.pixelGrid().getPixel(1, 0).getValue == 10)
    assert(rotatedImage.pixelGrid().getPixel(2, 0).getValue == 11)
    assert(rotatedImage.pixelGrid().getPixel(3, 0).getValue == 12)
    assert(rotatedImage.pixelGrid().getPixel(0, 1).getValue == 5)
    assert(rotatedImage.pixelGrid().getPixel(1, 1).getValue == 6)
    assert(rotatedImage.pixelGrid().getPixel(2, 1).getValue == 7)
    assert(rotatedImage.pixelGrid().getPixel(3, 1).getValue == 8)
    assert(rotatedImage.pixelGrid().getPixel(0, 2).getValue == 1)
    assert(rotatedImage.pixelGrid().getPixel(1, 2).getValue == 2)
    assert(rotatedImage.pixelGrid().getPixel(2, 2).getValue == 3)
    assert(rotatedImage.pixelGrid().getPixel(3, 2).getValue == 4)
  }

  test("Rotate rectangle image +90") {
    val rotateFilter = new RotateFilter(RotateAngle90())
    val rotatedImage = rotateFilter.filter(rectangleImage)

    /*
    09 10 11 12      01 05 09
    05 06 07 08  ->  02 06 10
    01 02 03 04      03 07 11
                     04 08 12 */
    assert(rotatedImage.height() == 4)
    assert(rotatedImage.width() == 3)
    assert(rotatedImage.pixelGrid().getPixel(0, 0).getValue == 1)
    assert(rotatedImage.pixelGrid().getPixel(1, 0).getValue == 5)
    assert(rotatedImage.pixelGrid().getPixel(2, 0).getValue == 9)
    assert(rotatedImage.pixelGrid().getPixel(0, 1).getValue == 2)
    assert(rotatedImage.pixelGrid().getPixel(1, 1).getValue == 6)
    assert(rotatedImage.pixelGrid().getPixel(2, 1).getValue == 10)
    assert(rotatedImage.pixelGrid().getPixel(0, 2).getValue == 3)
    assert(rotatedImage.pixelGrid().getPixel(1, 2).getValue == 7)
    assert(rotatedImage.pixelGrid().getPixel(2, 2).getValue == 11)
    assert(rotatedImage.pixelGrid().getPixel(0, 3).getValue == 4)
    assert(rotatedImage.pixelGrid().getPixel(1, 3).getValue == 8)
    assert(rotatedImage.pixelGrid().getPixel(2, 3).getValue == 12)
  }

  test("Rotate rectangle image +180") {
    val rotateFilter = new RotateFilter(RotateAngle180())
    val rotatedImage = rotateFilter.filter(rectangleImage)

    /*
    09 10 11 12      04 03 02 01
    05 06 07 08  ->  08 07 06 05
    01 02 03 04      12 11 10 09 */
    assert(rotatedImage.height() == 3)
    assert(rotatedImage.width() == 4)
    assert(rotatedImage.pixelGrid().getPixel(0, 0).getValue == 4)
    assert(rotatedImage.pixelGrid().getPixel(1, 0).getValue == 3)
    assert(rotatedImage.pixelGrid().getPixel(2, 0).getValue == 2)
    assert(rotatedImage.pixelGrid().getPixel(3, 0).getValue == 1)
    assert(rotatedImage.pixelGrid().getPixel(0, 1).getValue == 8)
    assert(rotatedImage.pixelGrid().getPixel(1, 1).getValue == 7)
    assert(rotatedImage.pixelGrid().getPixel(2, 1).getValue == 6)
    assert(rotatedImage.pixelGrid().getPixel(3, 1).getValue == 5)
    assert(rotatedImage.pixelGrid().getPixel(0, 2).getValue == 12)
    assert(rotatedImage.pixelGrid().getPixel(1, 2).getValue == 11)
    assert(rotatedImage.pixelGrid().getPixel(2, 2).getValue == 10)
    assert(rotatedImage.pixelGrid().getPixel(3, 2).getValue == 9)
  }

  test("Rotate rectangle image +270") {
    val rotateFilter = new RotateFilter(RotateAngle270())
    val rotatedImage = rotateFilter.filter(rectangleImage)

    /*
    09 10 11 12      12 08 04
    05 06 07 08  ->  11 07 03
    01 02 03 04      10 06 02
                     09 05 01 */
    assert(rotatedImage.height() == 4)
    assert(rotatedImage.width() == 3)
    assert(rotatedImage.pixelGrid().getPixel(0, 0).getValue == 12)
    assert(rotatedImage.pixelGrid().getPixel(1, 0).getValue == 8)
    assert(rotatedImage.pixelGrid().getPixel(2, 0).getValue == 4)
    assert(rotatedImage.pixelGrid().getPixel(0, 1).getValue == 11)
    assert(rotatedImage.pixelGrid().getPixel(1, 1).getValue == 7)
    assert(rotatedImage.pixelGrid().getPixel(2, 1).getValue == 3)
    assert(rotatedImage.pixelGrid().getPixel(0, 2).getValue == 10)
    assert(rotatedImage.pixelGrid().getPixel(1, 2).getValue == 6)
    assert(rotatedImage.pixelGrid().getPixel(2, 2).getValue == 2)
    assert(rotatedImage.pixelGrid().getPixel(0, 3).getValue == 9)
    assert(rotatedImage.pixelGrid().getPixel(1, 3).getValue == 5)
    assert(rotatedImage.pixelGrid().getPixel(2, 3).getValue == 1)
  }

}
