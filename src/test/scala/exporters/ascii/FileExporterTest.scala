package exporters.ascii

import models.grids.arrayPixelGrids.ASCIIPixelGrid
import models.images.ASCIIImage
import models.pixels.ASCIIPixel
import org.scalatest.FunSuite

import java.io.{File, FileInputStream}
import scala.io.Source

class FileExporterTest extends FunSuite {

  /*
  a b c
  d e f */
  val ASCIIPixelGrid = new ASCIIPixelGrid(2, 3)
  ASCIIPixelGrid.setPixel(0, 0, new ASCIIPixel('a'))
  ASCIIPixelGrid.setPixel(1, 0, new ASCIIPixel('b'))
  ASCIIPixelGrid.setPixel(2, 0, new ASCIIPixel('c'))
  ASCIIPixelGrid.setPixel(0, 1, new ASCIIPixel('d'))
  ASCIIPixelGrid.setPixel(1, 1, new ASCIIPixel('e'))
  ASCIIPixelGrid.setPixel(2, 1, new ASCIIPixel('f'))
  val ASCIIImage = new ASCIIImage(ASCIIPixelGrid)

  test ( "test" ) {
    val exporter = new FileExporter( "testFile.txt" )
    exporter.`export`( ASCIIImage )
    val file = new File ( "testFile.txt")

    val bufferedSource = Source.fromFile( file )
    val output = bufferedSource.mkString
    bufferedSource.close()
    file.delete()

    assert( output == "abc\ndef\n")
  }

}
