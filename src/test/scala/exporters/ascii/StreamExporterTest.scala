package exporters.ascii

import models.grids.arrayPixelGrids.ASCIIPixelGrid
import models.images.ASCIIImage
import models.pixels.ASCIIPixel
import org.scalatest.FunSuite

import java.io.ByteArrayOutputStream

class StreamExporterTest extends FunSuite {

  /*
  a b c
  d e f */
  val ASCIIPixelGrid = new ASCIIPixelGrid(2, 3)
  ASCIIPixelGrid.setPixel(0, 0, new ASCIIPixel('a'))
  ASCIIPixelGrid.setPixel(1, 0, new ASCIIPixel('b'))
  ASCIIPixelGrid.setPixel(2, 0, new ASCIIPixel('c'))
  ASCIIPixelGrid.setPixel(0, 1, new ASCIIPixel('d'))
  ASCIIPixelGrid.setPixel(1, 1, new ASCIIPixel('e'))
  ASCIIPixelGrid.setPixel(2, 1, new ASCIIPixel('f'))
  val ASCIIImage = new ASCIIImage(ASCIIPixelGrid)

  test ( "Basic ASCIIImage to stream export" ) {
    val outputStream = new ByteArrayOutputStream()
    val exporter = new StreamExporter( outputStream )
    exporter.`export`( ASCIIImage )
    assert ( outputStream.toString == "abc\ndef\n")
  }

  test("StreamExporter should throw an exception when trying to export after being closed") {
    val outputStream = new ByteArrayOutputStream()
    val exporter = new StreamExporter(outputStream)
    exporter.`export`(ASCIIImage)
    assertThrows[IllegalStateException](exporter.`export`(ASCIIImage))
  }
}
