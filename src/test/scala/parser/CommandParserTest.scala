package parser

import handlers.Handler
import org.scalatest.FunSuite

import scala.annotation.tailrec

class CommandParserTest extends FunSuite {

  private def stringToList(string: String): List[String] = {
    string.split(" ").toList
  }

  private def countHandlersInChain ( handler: Option[Handler[_,_]] ): Int = {

    if ( handler.isEmpty)
      return 0

    @tailrec
    def helper(handler: Handler[_,_], acc: Int ): Int = {

      if ( handler.nextHandler.isEmpty )
        acc+1
      else
        helper( handler.nextHandler.get, acc+1 )
    }
    helper( handler.get, 0)
  }

  test("More than one import argument - file and random") {
    val commandParser = new CommandParser()
    assertThrows[IllegalArgumentException](commandParser.parse(stringToList("--image src/resources/tests/test-image.png --image-random")))
  }

  test("More than one import argument - file and file") {
    val commandParser = new CommandParser()
    assertThrows[IllegalArgumentException](commandParser.parse(stringToList("--image src/resources/tests/test-image.png --image src/resources/tests/test-image.png")))
  }

  test("More than one import argument - random and random") {
    val commandParser = new CommandParser()
    assertThrows[IllegalArgumentException](commandParser.parse(stringToList("--image-random --image-random")))
  }

  test("Invalid argument") {
    val commandParser = new CommandParser()
    assertThrows[IllegalArgumentException](commandParser.parse(stringToList("--image src/resources/tests/test-image.png --rotate 90 --wrong")))
  }

  test("Invalid conversion table") {
    val commandParser = new CommandParser()
    assertThrows[IllegalArgumentException](commandParser.parse(stringToList("--image src/resources/tests/test-image.png --rotate 90 --table test")))
  }

  test("Invalid brightness filter argument") {
    val commandParser = new CommandParser()
    assertThrows[IllegalArgumentException](commandParser.parse(stringToList("--image src/resources/tests/test-image.png --brightness ab")))
  }

  test("Invalid rotate filter argument") {
    val commandParser = new CommandParser()
    assertThrows[IllegalArgumentException](commandParser.parse(stringToList("--image src/resources/tests/test-image.png --rotate 84")))
  }

  test("Invalid flip filter argument") {
    val commandParser = new CommandParser()
    assertThrows[IllegalArgumentException](commandParser.parse(stringToList("--image src/resources/tests/test-image.png --flip z")))
  }

  test("More than one conversion table - predefined table") {
    val commandParser = new CommandParser()
    assertThrows[IllegalArgumentException](commandParser.parse(stringToList("--image src/resources/tests/test-image.png --table linear --table nonlinear")))
  }

  test("More than one conversion table - custom table") {
    val commandParser = new CommandParser()
    assertThrows[IllegalArgumentException](commandParser.parse(stringToList("--image src/resources/tests/test-image.png --table linear --custom-table avbcjd")))
  }

  test("No image argument found") {
    val commandParser = new CommandParser()
    assertThrows[IllegalArgumentException](commandParser.parse(stringToList("--table linear --output-console --flip x")))
  }

  test("Valid flip arguments") {
    val commandParser = new CommandParser()
    commandParser.parse(stringToList("--image src/resources/tests/test-image.png --flip x --flip y"))

    assert( countHandlersInChain( commandParser.conversionHandler) == 1)
    assert( countHandlersInChain( commandParser.importHandler) == 1)
    assert( countHandlersInChain( commandParser.filterHandler) == 2)
    assert( countHandlersInChain( commandParser.exportHandler) == 0)
  }

  test("Valid positive rotate arguments") {
    val commandParser = new CommandParser()
    commandParser.parse(stringToList("--image src/resources/tests/test-image.png --rotate 0 --rotate 90 --rotate 180 --rotate 270 --rotate 360"))

    assert( countHandlersInChain( commandParser.conversionHandler) == 1)
    assert( countHandlersInChain( commandParser.importHandler) == 1)
    assert( countHandlersInChain( commandParser.filterHandler) == 5)
    assert( countHandlersInChain( commandParser.exportHandler) == 0)
  }

  test("Valid negative rotate arguments") {
    val commandParser = new CommandParser()
    commandParser.parse(stringToList("--image src/resources/tests/test-image.png --rotate -0 --rotate -90 --rotate -180 --rotate -270 --rotate -360 --output-file src/resources/tmp/outfile.txt"))

    assert( countHandlersInChain( commandParser.conversionHandler) == 1)
    assert( countHandlersInChain( commandParser.importHandler) == 1)
    assert( countHandlersInChain( commandParser.filterHandler) == 5)
    assert( countHandlersInChain( commandParser.exportHandler) == 1)
  }

  test("Valid nonlinear table arguments") {
    val commandParser = new CommandParser()
    commandParser.parse(stringToList("--image src/resources/tests/test-image.png --table non-linear --output-file src/resources/tmp/outfile.txt --output-console"))

    assert( countHandlersInChain( commandParser.conversionHandler) == 1)
    assert( countHandlersInChain( commandParser.importHandler) == 1)
    assert( countHandlersInChain( commandParser.filterHandler) == 0)
    assert( countHandlersInChain( commandParser.exportHandler) == 2)
  }

  test("Valid linear table arguments") {
    val commandParser = new CommandParser()
    commandParser.parse(stringToList("--image src/resources/tests/test-image.png --table linear --output-file src/resources/tmp/outfile.txt"))

    assert( countHandlersInChain( commandParser.conversionHandler) == 1)
    assert( countHandlersInChain( commandParser.importHandler) == 1)
    assert( countHandlersInChain( commandParser.filterHandler) == 0)
    assert( countHandlersInChain( commandParser.exportHandler) == 1)
  }

  test("All possible arguments with image from file") {
    val commandParser = new CommandParser()
    commandParser.parse(stringToList("--image src/resources/tests/test-image.png --flip x --flip y --rotate 90 --brightness 60 --invert --rotate -90 --table linear --output-console --output-file src/resources/tmp/outfile2.txt --output-file src/resources/tmp/outfile.txt"))

    assert( countHandlersInChain( commandParser.conversionHandler) == 1)
    assert( countHandlersInChain( commandParser.importHandler) == 1)
    assert( countHandlersInChain( commandParser.filterHandler) == 6)
    assert( countHandlersInChain( commandParser.exportHandler) == 3)
  }

  test("All possible arguments with random image") {
    val commandParser = new CommandParser()
    commandParser.parse(stringToList("--image-random --flip x --flip y --rotate 90 --brightness 60 --invert --rotate -90 --table linear --output-console --output-file src/resources/tmp/outfile2.txt --output-file src/resources/tmp/outfile.txt"))

    assert( countHandlersInChain( commandParser.conversionHandler) == 1)
    assert( countHandlersInChain( commandParser.importHandler) == 1)
    assert( countHandlersInChain( commandParser.filterHandler) == 6)
    assert( countHandlersInChain( commandParser.exportHandler) == 3)
  }
}
