package convertors.greyscaleToASCII

import models.grids.arrayPixelGrids.GreyscalePixelGrid
import models.images.GreyscaleImage
import models.pixels.GreyscalePixel
import org.scalatest.FunSuite

class LinearConvertorTest extends FunSuite {

  val squarePixelGrid = new GreyscalePixelGrid(3, 3)
  squarePixelGrid.setPixel(0, 0, new GreyscalePixel(0))
  squarePixelGrid.setPixel(1, 0, new GreyscalePixel(31))
  squarePixelGrid.setPixel(2, 0, new GreyscalePixel(32))
  squarePixelGrid.setPixel(0, 1, new GreyscalePixel(63))
  squarePixelGrid.setPixel(1, 1, new GreyscalePixel(159))
  squarePixelGrid.setPixel(2, 1, new GreyscalePixel(191))
  squarePixelGrid.setPixel(0, 2, new GreyscalePixel(193))
  squarePixelGrid.setPixel(1, 2, new GreyscalePixel(223))
  squarePixelGrid.setPixel(2, 2, new GreyscalePixel(255))
  val squareGreyscaleImage = new GreyscaleImage(squarePixelGrid)

  val rectanglePixelGrid = new GreyscalePixelGrid(1, 2)
  rectanglePixelGrid.setPixel(0, 0, new GreyscalePixel(0))
  rectanglePixelGrid.setPixel(1, 0, new GreyscalePixel(31))
  val rectangleGreyscaleImage = new GreyscaleImage(rectanglePixelGrid)

  test("Square Greyscale to ASCII conversion") {
    /*
    256/8 = 32
    0-31     -> a
    32-63    -> b
    64-95    -> c
    96-127   -> d
    128-159  -> e
    160-191  -> f
    192-223  -> g
    224-255  -> h
    */
    val linearConvertor = new LinearConvertor("abcdefgh")
    val ASCIIImage = linearConvertor.convert(squareGreyscaleImage)

    assert(ASCIIImage.height() == 3)
    assert(ASCIIImage.width() == 3)
    assert(ASCIIImage.pixelGrid().getPixel(0, 0).getValue == 'a')
    assert(ASCIIImage.pixelGrid().getPixel(1, 0).getValue == 'a')
    assert(ASCIIImage.pixelGrid().getPixel(2, 0).getValue == 'b')
    assert(ASCIIImage.pixelGrid().getPixel(0, 1).getValue == 'b')
    assert(ASCIIImage.pixelGrid().getPixel(1, 1).getValue == 'e')
    assert(ASCIIImage.pixelGrid().getPixel(2, 1).getValue == 'f')
    assert(ASCIIImage.pixelGrid().getPixel(0, 2).getValue == 'g')
    assert(ASCIIImage.pixelGrid().getPixel(1, 2).getValue == 'g')
    assert(ASCIIImage.pixelGrid().getPixel(2, 2).getValue == 'h')
  }

  test("Rectangle Greyscale to ASCII conversion") {
    /*
    256/8 = 32
    0-31     -> a
    32-63    -> b
    */
    val linearConvertor = new LinearConvertor("abcdefgh")
    val ASCIIImage = linearConvertor.convert(rectangleGreyscaleImage)

    assert(ASCIIImage.height() == 1)
    assert(ASCIIImage.width() == 2)
    assert(ASCIIImage.pixelGrid().getPixel(0, 0).getValue == 'a')
    assert(ASCIIImage.pixelGrid().getPixel(1, 0).getValue == 'a')
  }

  test("Wrong conversion table - empty") {
     assertThrows[IllegalArgumentException](new LinearConvertor(""))
  }
}