package convertors.greyscaleToASCII

import models.grids.arrayPixelGrids.GreyscalePixelGrid
import models.images.GreyscaleImage
import models.pixels.GreyscalePixel
import org.scalatest.FunSuite

class NonLinearConvertorTest extends FunSuite {

  val pixelGrid = new GreyscalePixelGrid(3, 3)
  pixelGrid.setPixel(0, 0, new GreyscalePixel(124))
  pixelGrid.setPixel(1, 0, new GreyscalePixel(31))
  pixelGrid.setPixel(2, 0, new GreyscalePixel(231))
  pixelGrid.setPixel(0, 1, new GreyscalePixel(63))
  pixelGrid.setPixel(1, 1, new GreyscalePixel(159))
  pixelGrid.setPixel(2, 1, new GreyscalePixel(191))
  pixelGrid.setPixel(0, 2, new GreyscalePixel(193))
  pixelGrid.setPixel(1, 2, new GreyscalePixel(21))
  pixelGrid.setPixel(2, 2, new GreyscalePixel(0))
  val greyscaleImage = new GreyscaleImage(pixelGrid)


  test("Normal Greyscale to ASCII conversion") {

    val convertor = new NonLinearConvertor(Array(
      ('a', 0, 100),
      ('b', 101, 131),
      ('c', 132, 150),
      ('d', 151, 255)
    ))

    val ASCIIImage = convertor.convert(greyscaleImage)

    assert(ASCIIImage.height() == 3)
    assert(ASCIIImage.width() == 3)
    assert(ASCIIImage.pixelGrid().getPixel(0, 0).getValue == 'b')
    assert(ASCIIImage.pixelGrid().getPixel(1, 0).getValue == 'a')
    assert(ASCIIImage.pixelGrid().getPixel(2, 0).getValue == 'd')
    assert(ASCIIImage.pixelGrid().getPixel(0, 1).getValue == 'a')
    assert(ASCIIImage.pixelGrid().getPixel(1, 1).getValue == 'd')
    assert(ASCIIImage.pixelGrid().getPixel(2, 1).getValue == 'd')
    assert(ASCIIImage.pixelGrid().getPixel(0, 2).getValue == 'd')
    assert(ASCIIImage.pixelGrid().getPixel(1, 2).getValue == 'a')
    assert(ASCIIImage.pixelGrid().getPixel(2, 2).getValue == 'a')

  }

  test("Wrong conversion table - intervals overlapping") {

    assertThrows[IllegalArgumentException](new NonLinearConvertor(Array(
      ('a', 0, 100),
      ('b', 101, 131),
      ('c', 132, 150),
      ('d', 150, 255)
    )))
  }

  test("Wrong conversion table - value missing") {

    assertThrows[IllegalArgumentException](new NonLinearConvertor(Array(
      ('a', 0, 100),
      ('b', 102, 131),
      ('c', 132, 150),
      ('d', 151, 255)
    )))
  }
}
