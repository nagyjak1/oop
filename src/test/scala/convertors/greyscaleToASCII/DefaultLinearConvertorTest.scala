package convertors.greyscaleToASCII

import models.grids.arrayPixelGrids.GreyscalePixelGrid
import models.images.GreyscaleImage
import models.pixels.GreyscalePixel
import org.scalatest.FunSuite

class DefaultLinearConvertorTest extends FunSuite {

  val pixelGrid = new GreyscalePixelGrid(3, 3)
  pixelGrid.setPixel(0, 0, new GreyscalePixel(124))
  pixelGrid.setPixel(1, 0, new GreyscalePixel(31))
  pixelGrid.setPixel(2, 0, new GreyscalePixel(231))
  pixelGrid.setPixel(0, 1, new GreyscalePixel(63))
  pixelGrid.setPixel(1, 1, new GreyscalePixel(159))
  pixelGrid.setPixel(2, 1, new GreyscalePixel(191))
  pixelGrid.setPixel(0, 2, new GreyscalePixel(193))
  pixelGrid.setPixel(1, 2, new GreyscalePixel(21))
  pixelGrid.setPixel(2, 2, new GreyscalePixel(0))
  val greyscaleImage = new GreyscaleImage(pixelGrid)


  /*
   .:-=+*#%@
  256/10 = 25,6
  0-25     -> ' '
  26-51    -> '.'
  52-76    -> ':'
  77-102   -> '-'
  103-127  -> '='
  128-153  -> '+'
  154-179  -> '*'
  180-204  -> '#'
  205-230  -> '%'
  231-255  -> '@'
  */

  test ("Convert square image to ASCII") {

    val linearConvertor = new DefaultLinearConvertor()
    val ASCIIImage = linearConvertor.convert( greyscaleImage )

    assert(ASCIIImage.height() == 3)
    assert(ASCIIImage.width() == 3)
    assert(ASCIIImage.pixelGrid().getPixel(0, 0).getValue == '=')
    assert(ASCIIImage.pixelGrid().getPixel(1, 0).getValue == '.')
    assert(ASCIIImage.pixelGrid().getPixel(2, 0).getValue == '@')
    assert(ASCIIImage.pixelGrid().getPixel(0, 1).getValue == ':')
    assert(ASCIIImage.pixelGrid().getPixel(1, 1).getValue == '*')
    assert(ASCIIImage.pixelGrid().getPixel(2, 1).getValue == '#')
    assert(ASCIIImage.pixelGrid().getPixel(0, 2).getValue == '#')
    assert(ASCIIImage.pixelGrid().getPixel(1, 2).getValue == ' ')
    assert(ASCIIImage.pixelGrid().getPixel(2, 2).getValue == ' ')

  }

}
