package convertors.greyscaleToASCII

import models.grids.arrayPixelGrids.GreyscalePixelGrid
import models.images.GreyscaleImage
import models.pixels.GreyscalePixel
import org.scalatest.FunSuite

class DefaultNonLinearConvertorTest extends FunSuite {

  val pixelGrid = new GreyscalePixelGrid(3, 3)
  pixelGrid.setPixel(0, 0, new GreyscalePixel(124))
  pixelGrid.setPixel(1, 0, new GreyscalePixel(31))
  pixelGrid.setPixel(2, 0, new GreyscalePixel(231))
  pixelGrid.setPixel(0, 1, new GreyscalePixel(63))
  pixelGrid.setPixel(1, 1, new GreyscalePixel(159))
  pixelGrid.setPixel(2, 1, new GreyscalePixel(191))
  pixelGrid.setPixel(0, 2, new GreyscalePixel(193))
  pixelGrid.setPixel(1, 2, new GreyscalePixel(21))
  pixelGrid.setPixel(2, 2, new GreyscalePixel(0))
  val greyscaleImage = new GreyscaleImage(pixelGrid)

  test("Convert square image to ASCII") {

    /*
    DEFAULT VALUES:
    ('*', 0, 50),
    ('.', 51, 132),
    ('-',133,178),
    ('@',179,219),
    ('^',220,230),
    ('!',231,240),
    ('~',241,249),
    ('@',250,255))
    */
    val nonlinearConvertor = new DefaultNonLinearConvertor()
    val ASCIIImage = nonlinearConvertor.convert( greyscaleImage )

    assert(ASCIIImage.height() == 3)
    assert(ASCIIImage.width() == 3)
    assert(ASCIIImage.pixelGrid().getPixel(0, 0).getValue == '.')
    assert(ASCIIImage.pixelGrid().getPixel(1, 0).getValue == '*')
    assert(ASCIIImage.pixelGrid().getPixel(2, 0).getValue == '!')
    assert(ASCIIImage.pixelGrid().getPixel(0, 1).getValue == '.')
    assert(ASCIIImage.pixelGrid().getPixel(1, 1).getValue == '-')
    assert(ASCIIImage.pixelGrid().getPixel(2, 1).getValue == '@')
    assert(ASCIIImage.pixelGrid().getPixel(0, 2).getValue == '@')
    assert(ASCIIImage.pixelGrid().getPixel(1, 2).getValue == '*')
    assert(ASCIIImage.pixelGrid().getPixel(2, 2).getValue == '*')
  }
}
