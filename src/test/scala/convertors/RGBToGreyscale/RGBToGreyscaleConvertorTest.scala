package convertors.RGBToGreyscale

import models.grids.arrayPixelGrids.RGBPixelGrid
import models.images.RGBImage
import models.pixels.RGBPixel
import org.scalatest.FunSuite

class RGBToGreyscaleConvertorTest extends FunSuite {

  val squarePixelGrid = new RGBPixelGrid(3, 3)
  squarePixelGrid.setPixel(0, 0, new RGBPixel(0xff010101))
  squarePixelGrid.setPixel(1, 0, new RGBPixel(0xff111111))
  squarePixelGrid.setPixel(2, 0, new RGBPixel(0xff101010))
  squarePixelGrid.setPixel(0, 1, new RGBPixel(0xff333333))
  squarePixelGrid.setPixel(1, 1, new RGBPixel(0xff444444))
  squarePixelGrid.setPixel(2, 1, new RGBPixel(0xffffffff))
  squarePixelGrid.setPixel(0, 2, new RGBPixel(0xff000000))
  squarePixelGrid.setPixel(1, 2, new RGBPixel(0xff050505))
  squarePixelGrid.setPixel(2, 2, new RGBPixel(0xff333333))
  val squareRGBImage = new RGBImage(squarePixelGrid)


  val rectanglePixelGrid = new RGBPixelGrid(3, 4)
  rectanglePixelGrid.setPixel(0, 0, new RGBPixel(0xff010101))
  rectanglePixelGrid.setPixel(1, 0, new RGBPixel(0xff111111))
  rectanglePixelGrid.setPixel(2, 0, new RGBPixel(0xff101010))
  rectanglePixelGrid.setPixel(3, 0, new RGBPixel(0xff101ff0))
  rectanglePixelGrid.setPixel(0, 1, new RGBPixel(0xff333333))
  rectanglePixelGrid.setPixel(1, 1, new RGBPixel(0xff444444))
  rectanglePixelGrid.setPixel(2, 1, new RGBPixel(0xffffffff))
  rectanglePixelGrid.setPixel(3, 1, new RGBPixel(0xff10ff10))
  rectanglePixelGrid.setPixel(0, 2, new RGBPixel(0xff000000))
  rectanglePixelGrid.setPixel(1, 2, new RGBPixel(0xff050505))
  rectanglePixelGrid.setPixel(2, 2, new RGBPixel(0xff333333))
  rectanglePixelGrid.setPixel(3, 2, new RGBPixel(0xff111110))
  val rectangleRBGImage = new RGBImage(rectanglePixelGrid)


  private def RGBToGreyscaleFormula(rgb: Int): Int = {
    val red = (rgb & 0x00FF0000) >> 16
    val green = (rgb & 0x0000FF00) >> 8
    val blue = rgb & 0x000000FF

    val greyScale = (0.3 * red) + (0.59 * green) + (0.11 * blue)
    if (greyScale >= 255)
      255
    else
      greyScale.round.toInt
  }


  test("Square RGBImage to GreyscaleImage") {
    val convertor = new RGBToGreyscaleConvertor()
    val greyscaleImage = convertor.convert(squareRGBImage)

    assert(greyscaleImage.height() == squareRGBImage.height())
    assert(greyscaleImage.width() == squareRGBImage.width())

    for (i <- 0 until greyscaleImage.height) {
      for (j <- 0 until greyscaleImage.width) {
        assert(greyscaleImage.pixelGrid().getPixel(j, i).getValue ==
          RGBToGreyscaleFormula(squareRGBImage.pixelGrid().getPixel(j, i).getValue))
      }
    }
  }


  test("Rectangle RGBImage to GreyscaleImage") {
    val convertor = new RGBToGreyscaleConvertor()
    val greyscaleImage = convertor.convert(rectangleRBGImage)

    assert(greyscaleImage.height() == rectangleRBGImage.height())
    assert(greyscaleImage.width() == rectangleRBGImage.width())

    for (i <- 0 until greyscaleImage.height) {
      for (j <- 0 until greyscaleImage.width) {
        assert(greyscaleImage.pixelGrid().getPixel(j, i).getValue ==
          RGBToGreyscaleFormula(rectangleRBGImage.pixelGrid().getPixel(j, i).getValue))
      }
    }
  }
}
