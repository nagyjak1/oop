package convertors.ASCItoString

import convertors.ASCIIToString.ASCIIToStringConvertor
import models.grids.arrayPixelGrids.ASCIIPixelGrid
import models.images.ASCIIImage
import models.pixels.ASCIIPixel
import org.scalatest.FunSuite

class ASCIToStringConvertorTest extends FunSuite {

  /*
  a b c
  d e f */
  val rectangleASCIIPixelGrid = new ASCIIPixelGrid(2, 3)
  rectangleASCIIPixelGrid.setPixel(0, 0, new ASCIIPixel('a'))
  rectangleASCIIPixelGrid.setPixel(1, 0, new ASCIIPixel('b'))
  rectangleASCIIPixelGrid.setPixel(2, 0, new ASCIIPixel('c'))
  rectangleASCIIPixelGrid.setPixel(0, 1, new ASCIIPixel('d'))
  rectangleASCIIPixelGrid.setPixel(1, 1, new ASCIIPixel('e'))
  rectangleASCIIPixelGrid.setPixel(2, 1, new ASCIIPixel('f'))
  val rectangleASCIIImage = new ASCIIImage(rectangleASCIIPixelGrid)

  /*
  a b c
  d e f
  g h i
  */
  val squareASCIIPixelGrid = new ASCIIPixelGrid(3, 3)
  squareASCIIPixelGrid.setPixel(0, 0, new ASCIIPixel('a'))
  squareASCIIPixelGrid.setPixel(1, 0, new ASCIIPixel('b'))
  squareASCIIPixelGrid.setPixel(2, 0, new ASCIIPixel('c'))
  squareASCIIPixelGrid.setPixel(0, 1, new ASCIIPixel('d'))
  squareASCIIPixelGrid.setPixel(1, 1, new ASCIIPixel('e'))
  squareASCIIPixelGrid.setPixel(2, 1, new ASCIIPixel('f'))
  squareASCIIPixelGrid.setPixel(0, 2, new ASCIIPixel('g'))
  squareASCIIPixelGrid.setPixel(1, 2, new ASCIIPixel('h'))
  squareASCIIPixelGrid.setPixel(2, 2, new ASCIIPixel('i'))
  val squareASCIIImage = new ASCIIImage(squareASCIIPixelGrid)


  test("Square ASCIIImage to String conversion") {
    val convertor = new ASCIIToStringConvertor()
    val output = convertor.convert(squareASCIIImage)

    assert(output == "abc\ndef\nghi\n")
  }

  test("Rectangle ASCIIImage to String conversion") {
    val convertor = new ASCIIToStringConvertor()
    val output = convertor.convert(squareASCIIImage)

    assert(output == "abc\ndef\nghi\n")
  }

}
